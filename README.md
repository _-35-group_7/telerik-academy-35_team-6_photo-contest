# Telerik Academy А35_Team 6_Photo Contest

This is the final project for the A35 Telerik Academy cohort. It's an application that can allow a group of photographers to easily manage online photo contests. The app would allow photo junkies to participate in contests with photos.


## Description

You start with registering as a user. Admins/Contest administrators have extra priviliges and access on their UI.
Users may apply for contests, which administrators have created. The users apply by inserting a photo under a category, adding a name and description of the photo. The administrators may set a deadline for the specific contest.
There's 2 stages of a photo contest - I stage being for applying with a photo and the II stage for a jury (part of the administrators) to rate the photos. During Stage II a user may not upload an entry anymore. At the end of the II stage each user, who's photo had been rated, receives points, determined by the rating they received.


## Project status
In development until 22nd of June.
