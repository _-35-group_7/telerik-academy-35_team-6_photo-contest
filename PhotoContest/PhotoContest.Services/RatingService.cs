﻿using AutoMapper;
using PhotoContest.Data.Exceptions;
using PhotoContest.Domain.Contracts.Repositories;
using PhotoContest.Domain.Contracts.Services;
using PhotoContest.Domain.DataTransferObjects;
using PhotoContest.Domain.ViewModels.RatingViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhotoContest.Services
{
    public class RatingService : BaseService <RatingDTO, IRatingRepository>, IRatingService
    {
        private readonly IUserRepository _userRepository;
        private readonly IPhotoService _photoService;
        private readonly IMapper _mapper;

        public RatingService(IRatingRepository ratingRepository, IUserRepository userRepository,
            IPhotoService photoService, IMapper mapper) : base(ratingRepository)
        {
            _userRepository = userRepository;
            _photoService = photoService;
            _mapper = mapper;
        }

        public override async Task<RatingDTO> Create(RatingDTO dto)
        {
            var photo = await _photoService.GetById(dto.PhotoId);
            photo.Id = dto.PhotoId;
            if (photo.Contest.Phase != Domain.Enums.Stage.PhaseII)
            {
                throw new ExpiredStageException("You can not post ratings on this phase.");
            }

            dto.CreatedOn = DateTime.Now;

            var ratingToCreate = await _repository.Create(dto);

            _photoService.AddScore(dto.Score, photo);
            

            if (ratingToCreate.IsReviewed == false)
            {
                throw new InvalidEntryException("The entry's category is invalid, score 0 is assigned!");
            }

            return ratingToCreate;

        }

    }
}
