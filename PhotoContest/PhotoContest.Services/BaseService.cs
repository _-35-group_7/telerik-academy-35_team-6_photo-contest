﻿
using PhotoContest.Data.Exceptions;
using PhotoContest.Domain.Contracts.Repositories;
using PhotoContest.Domain.Contracts.Services;
using PhotoContest.Domain.DataTransferObjects;
using PhotoContest.Domain.Paging;
using System;
using System.Threading.Tasks;

namespace PhotoContest.Services
{
    public abstract class BaseService<TDto, TRepository>
                        : IBaseService<TDto, TRepository>
                          where TDto : BaseDTO
                          where TRepository : IBaseRepository<TDto>
    {
        internal readonly TRepository _repository;

        protected BaseService(TRepository repository)
        {
            _repository = repository; 
        }

        public virtual async Task<TDto> Create(TDto dto)
        {
            dto.CreatedOn = DateTime.Now;

            return await _repository.Create(dto);
            
        }
        public virtual async Task<bool> Delete(int id)
        {
            return await _repository.Delete(id);
        }

        public virtual Task<TDto> GetById(int id)
        {
            return _repository.GetById(id);    
        }

        public virtual async Task<TDto> Update(TDto dto)
        {
            var repo = await _repository.GetById(dto.Id);
            
            if (repo == null)
            {
                throw new EntityNotFoundException();
            }

            return await _repository.Update(dto);
        }

    }
}
