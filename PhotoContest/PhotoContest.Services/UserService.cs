﻿using PhotoContest.Domain.Contracts.Repositories;
using PhotoContest.Domain.Contracts.Services;
using PhotoContest.Domain.DataTransferObjects;
using System.Threading.Tasks;
using System;
using PhotoContest.Domain.Enums;
using PhotoContest.Domain.Paging;
using PhotoContest.Data.Exceptions;

namespace PhotoContest.Services
{
    public class UserService : BaseService<UserDTO, IUserRepository>, IUserService
    {

        public UserService(IUserRepository repository) : base(repository)
        {
            
        }

        public virtual async Task<PaginatedList<UserDTO>> GetPaginated(PagingOptions options)
        {
            return await _repository.GetPaginated(options);
        }

        public async Task<UserDTO> GetByUsername(string username)
        {
            try
            {
               return await _repository.GetByUsername(username);
            }
            catch (EntityNotFoundException e)
            {
                throw new EntityNotFoundException(e.Message);
            }
        }

        public override async Task<UserDTO> Create(UserDTO dto)
        {
            await _repository.DuplicateEmailAsync(dto.Email);
            await _repository.DuplicateUsernameAsync(dto.Username);

            dto.CreatedOn = DateTime.Now;

            return await _repository.Create(dto);
        }

    }
}
