﻿using PhotoContest.Domain.Contracts.Repositories;
using PhotoContest.Domain.Contracts.Services;
using PhotoContest.Domain.DataTransferObjects;
using PhotoContest.Domain.Paging;
using System.Threading.Tasks;

namespace PhotoContest.Services
{
    public class ContestService : BaseService<ContestDTO, IContestRepository>, IContestService
    {
        public readonly IUserService _userservice;
        public ContestService(IContestRepository repository, IUserService userservice) : base(repository)
        {
            _userservice = userservice;
        }

        public override async Task<ContestDTO> GetById(int id)
        {
            return await _repository.GetById(id);
        }

        public virtual async Task<PaginatedList<ContestDTO>> GetPaginated(PagingOptions options)
        {
            return await _repository.GetPaginated(options);
        }
        public async Task<bool> CheckIfParticipantInContestAsync(string username, int contestId)
        {
            var user = await _userservice.GetByUsername(username);
            var contest = await GetById(contestId);

            return contest.Partakers.Contains(user);
        }
    }
}
