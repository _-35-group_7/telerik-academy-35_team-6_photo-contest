﻿using AutoMapper;
using PhotoContest.Data.Exceptions;
using PhotoContest.Domain.Contracts.Repositories;
using PhotoContest.Domain.Contracts.Services;
using PhotoContest.Domain.DataTransferObjects;
using PhotoContest.Domain.Enums;
using PhotoContest.Domain.Paging;
using PhotoContest.Domain.ViewModels.PhotoViewModels;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PhotoContest.Services
{
    public class PhotoService : BaseService<PhotoDTO, IPhotoRepository>, IPhotoService
    {
        public readonly IUserService _userservice;
        public readonly IContestService _contestservice;
        private readonly IMapper _mapper;
        public PhotoService(IPhotoRepository repository, IUserService userservice, IContestService contestservice, IMapper mapper) : base(repository)
        {
            _userservice = userservice;
            _contestservice = contestservice;
            _mapper = mapper;
        }

        public virtual async Task<PaginatedList<PhotoDTO>> GetPaginated(PagingOptions options)
        {
            return await _repository.GetPaginated(options);
        }

        public async Task<PhotoGetViewModel> Create(PhotoPostViewModel dto)
        {
            var user = await _userservice.GetById(dto.UserId);
            var contest = await _contestservice.GetById(dto.ContestId);

            if (contest.Phase != Stage.PhaseI)
            {
                throw new ExpiredStageException("Phase I has already ended. Photo uploading is not allowed.");
            }

            var input = _mapper.Map<PhotoDTO>(dto);
            input.User = user;
            input.Contest = contest;

           /* foreach (var photo in contest.Photos)
            {
                if (photo.PhotoFile == dto.PhotoFile)
                {
                    throw new DuplicateEntityException("An instance of this photo already exists.");
                }
            }*/

            var output = await _repository.Create(input);

            PhotoGetViewModel result = new PhotoGetViewModel()
            {
                Title = output.Title,
                Story = output.Story,
                PhotoFile = output.PhotoFile,
                PhotoUrl = output.PhotoUrl,
                Username = output.User.Username,
                ContestTitle = output.Contest.Title,
            };
            return result;
        }

        public override Task<PhotoDTO> GetById(int id)
        {
            return this._repository.GetById(id);
        }
        public async Task<List<PhotoGetViewModel>> GetPhotosForContest(int contestId)
        {
            var contest = await _contestservice.GetById(contestId);
            var photos = contest.Photos.Select(p => _mapper.Map<PhotoGetViewModel>(p)).ToList();
           
            return photos;
        }
        public void AddScore(int score,PhotoDTO photoDTO)
        {
            this._repository.AddScore(score, photoDTO);
        }

    }
}
