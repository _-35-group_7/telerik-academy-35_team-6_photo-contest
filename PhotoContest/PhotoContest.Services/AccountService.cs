﻿using AutoMapper;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using PhotoContest.Data;
using PhotoContest.Data.Entities;
using PhotoContest.Data.Exceptions;
using PhotoContest.Data.Helpers;
using PhotoContest.Domain.Contracts.Services;
using PhotoContest.Domain.DataTransferObjects;
using PhotoContest.Services.HelperClasses;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace PhotoContest.Services
{
    public class AccountService : IAccountService
    {
        private readonly JwtSettings _settings;
        private readonly IUserService _userService;
        private readonly DataContext _context;
        private readonly IMapper _mapper;

        public AccountService(JwtSettings settings, IUserService userService, DataContext context, IMapper mapper)
        {
            _settings = settings;
            _userService = userService;
            _context = context;
            _mapper = mapper;
        }

        public string GenerateJwtToken(UserDTO user)
        {
            var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_settings.Key));
            var credentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256);

            var claims = new List<Claim> {
                new Claim(JwtRegisteredClaimNames.Sub, user.Username),
                new Claim(JwtRegisteredClaimNames.Aud, _settings.Audience),
                new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                new Claim(JwtRegisteredClaimNames.Iat, new DateTimeOffset(DateTime.UtcNow).ToUnixTimeSeconds().ToString()),
                new Claim(ClaimTypes.Name, user.Id.ToString())
            };

            /*foreach (var role in user.Roles)
            {
                if (role.Name == "User")
                {
                    claims.Add(new Claim(ClaimTypes.Role, "User"));
                }
                else if (role.Name == "Admin")
                {
                    claims.Add(new Claim(ClaimTypes.Role, "Admin"));
                }
            }*/

            var token = new JwtSecurityToken(
                issuer: _settings.Issuer,
                audience: null,
                claims: claims,

                expires: DateTime.UtcNow.Add(TimeSpan.FromMinutes(_settings.ExpirationInMinutes)),
                signingCredentials: credentials);

            return new JwtSecurityTokenHandler().WriteToken(token);
        }


        public UserDTO Login(string username, string password)
        {
            var user = _userService.GetByUsername(username);

            if (PasswordHasher.VerifyPassword(password, user.Result.Password))
            {
                return user.Result;
            }
            else
            {
                throw new InvalidCredentialsException();
            }
        }

        public async Task Register(UserDTO user, string password)
        {
            user.PasswordHash = PasswordHasher.HashPassword(password);

            await _userService.Create(user);
        }

    }
}
