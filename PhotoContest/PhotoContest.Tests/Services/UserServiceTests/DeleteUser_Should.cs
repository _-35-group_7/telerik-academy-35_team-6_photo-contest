﻿using AutoMapper;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using PhotoContest.Domain.Contracts.Repositories;
using PhotoContest.Domain.Contracts.Services;
using PhotoContest.Domain.DataTransferObjects;
using PhotoContest.Domain.Enums;
using PhotoContest.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhotoContest.Tests.Services.UserServiceTests
{
    [TestClass]
    public class DeleteUser_Should
    {
        private readonly UserService _sut;
        private readonly Mock<IUserRepository> _userRepository = new Mock<IUserRepository>();

        public DeleteUser_Should()
        {
            _sut = new UserService(_userRepository.Object);
        }

        [TestMethod]
        public async Task DeleteUser_When_ParamsAreValid()
        {
            //Arrange
            var userId = 1;
            var userDto = new UserDTO
            {
                Id = 1,
                FirstName = "TestUser",
                LastName = "TestedUser",
                Username = "t.user.1",
                Password = "1234567",
                Email = "t.user@gmail.com",
            };

            _userRepository.Setup(x => x.Delete(userId))
                .Returns(Task.FromResult(true));

            //Act
            bool userDeleted = await _sut.Delete(userDto.Id);

            //Assert
            Assert.AreEqual(expected: true, actual: userDeleted);
        }

        [TestMethod]
        public async Task ThrowEntityNotFoundException_When_PhotoNotFound()
        {
            // Arrange
            var userDto = new UserDTO
            {
                Id = 1,
                FirstName = "TestUser",
                LastName = "TestedUser",
                Username = "t.user.1",
                Password = "1234567",
                Email = "t.user@gmail.com",
            };

            _userRepository.Setup(x => x.Delete(int.MaxValue))
                .Returns(Task.FromResult(false));

            //Act
            bool userDeleted = await _sut.Delete(userDto.Id);

            //Assert
            Assert.AreEqual(expected: false, actual: userDeleted);
        }
    }
}
