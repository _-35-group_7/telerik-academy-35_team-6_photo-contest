﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using PhotoContest.Data.Exceptions;
using PhotoContest.Domain.Contracts.Repositories;
using PhotoContest.Domain.DataTransferObjects;
using PhotoContest.Domain.Enums;
using PhotoContest.Services;
using System;
using System.Threading.Tasks;

namespace PhotoContest.Tests.Services.UserServiceTests
{
    [TestClass]
    public class GetById_Should
    {
        private readonly UserService _sut;
        private readonly Mock<IUserRepository> _userRepository = new Mock<IUserRepository>();

        public GetById_Should()
        {
            _sut = new UserService(_userRepository.Object);
        }

        [TestMethod]
        public async Task GetById_ShouldReturnUser_WhenUserExists()
        {
            //Arrange
            var userId = 1;
            var userDto = new UserDTO
            {
               Id = 1,
               FirstName = "TestUser",
               LastName = "TestedUser",
               Username = "t.user.1",
               Password = "1234567",
               Email = "t.user@gmail.com",
            };

            _userRepository.Setup(x => x.GetById(It.IsAny<int>()))
                .ReturnsAsync(userDto);

            //Act
            UserDTO user = await _sut.GetById(userId);

            //Assert
            Assert.AreEqual(expected: userId, actual: user.Id);
            Assert.AreEqual(expected: userDto.FirstName, actual: user.FirstName);
            Assert.AreEqual(expected: userDto.LastName, actual: user.LastName);
            Assert.AreEqual(expected: userDto.Username, actual: user.Username);
            Assert.AreEqual(expected: userDto.Password, actual: user.Password);
            Assert.AreEqual(expected: userDto.Email, actual: user.Email);
            Assert.AreEqual(expected: userDto.Role, actual: user.Role);

        }

        [TestMethod]
        public async Task GetById_ShouldReturnNothing_WhenUserDoesNotExist()
        {
            //Arrange
            var userId = 1;

            _userRepository.Setup(x => x.GetById(It.IsAny<int>()))
                .ReturnsAsync(() => null);

            //Act
            UserDTO user = await _sut.GetById(userId);

            //Assert
            Assert.IsNull(user);
        }

        [TestMethod]
        public async Task GetById_ShouldReturnOnce_WhenUserExists()
        {
            //Arrange
            var userId = 1;
            var userDto = new UserDTO
            {
                Id = 1,
                FirstName = "TestUser",
                LastName = "TestedUser",
                Username = "t.user.1",
                Password = "1234567",
                Email = "t.user@gmail.com",
            };

            _userRepository.Setup(x => x.GetById(It.IsAny<int>()))
                .ReturnsAsync(userDto);

            //Act
            UserDTO user = await _sut.GetById(userId);

            //Assert
            _userRepository.Verify(x => x.GetById(userId), Times.Once);
        }

        [TestMethod]
        public void GetByUsername_ShouldThrowException_WhenUserDoesNotExist()
        {
            //Arrange
            var userDto = new UserDTO
            {
                Id = 1,
                FirstName = "TestUser",
                LastName = "TestedUser",
                Username = "t.user.1",
                Password = "1234567",
                Email = "t.user@gmail.com",
            };

             _userRepository.Setup(x => x.GetByUsername("Peter"))
              .Throws<EntityNotFoundException>();
        }

    }
}
