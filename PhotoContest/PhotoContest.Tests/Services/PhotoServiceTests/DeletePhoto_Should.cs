﻿using AutoMapper;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using PhotoContest.Domain.Contracts.Repositories;
using PhotoContest.Domain.Contracts.Services;
using PhotoContest.Domain.DataTransferObjects;
using PhotoContest.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhotoContest.Tests.Services.PhotoServiceTests
{
    [TestClass]
    public class DeletePhoto_Should
    {
        private readonly Mock<IPhotoRepository> _photoRepository = new Mock<IPhotoRepository>();
        private readonly Mock<IContestService> _contestService = new Mock<IContestService>();
        private readonly Mock<IUserService> _userService = new Mock<IUserService>();
        private readonly Mock<IMapper> _mapper = new Mock<IMapper>();
        private readonly PhotoService _sut;

        public DeletePhoto_Should()
        {
            _sut = new PhotoService(_photoRepository.Object, _userService.Object, _contestService.Object, _mapper.Object);
        }

        [TestMethod]
        public async Task DeletePhoto_When_ParamsAreValid()
        {
            //Arrange
            var photoId = 1;
            var photoDto = new PhotoDTO
            {
                Id = photoId,
                Title = "Cassy",
                PhotoFile = null,
                ContestId = 1,
                UserId = 1
            };

            _photoRepository.Setup(x => x.Delete(photoId))
                .Returns(Task.FromResult(true));

            //Act
            bool photoDeleted = await _sut.Delete(photoDto.Id);

            //Assert
            Assert.AreEqual(expected: true, actual: photoDeleted);
        }

        [TestMethod]
        public async Task ThrowEntityNotFoundException_When_PhotoNotFound()
        {
            // Arrange
            var photoId = 1;
            var photoDto = new PhotoDTO
            {
                Id = photoId,
                Title = "Cassy",
                PhotoFile = null,
                ContestId = 1,
                UserId = 1
            };

            _photoRepository.Setup(x => x.Delete(int.MaxValue))
                .Returns(Task.FromResult(false));

            //Act
            bool photoDeleted = await _sut.Delete(photoDto.Id);

            //Assert
            Assert.AreEqual(expected: false, actual: photoDeleted);
        }
    }
}
