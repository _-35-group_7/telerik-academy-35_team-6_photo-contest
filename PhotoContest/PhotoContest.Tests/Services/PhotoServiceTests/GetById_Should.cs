using AutoMapper;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using PhotoContest.Domain.Contracts.Repositories;
using PhotoContest.Domain.Contracts.Services;
using PhotoContest.Domain.DataTransferObjects;
using PhotoContest.Domain.Enums;
using PhotoContest.Services;
using System;
using System.Threading.Tasks;

namespace PhotoContest.Tests.Services.PhotoServiceTests
{
    [TestClass]
    public class GetById_Should
    {
        
        private readonly PhotoService _sut;

        private readonly Mock<IPhotoRepository> _photoRepository = new Mock<IPhotoRepository>();
        private readonly Mock<IUserService> _userService = new Mock<IUserService>();
        private readonly Mock<IContestService> _contestService = new Mock<IContestService>();
        private readonly Mock<IMapper> _mapper = new Mock<IMapper>();

        public GetById_Should()
        {
            _sut = new PhotoService(_photoRepository.Object, _userService.Object, _contestService.Object,
               _mapper.Object);
        }

        [TestMethod]
        public async Task GetById_ShouldReturnPhoto_WhenPhotoExists()
        {
            //Arrange
            var photoId = 1;
            var photoDto = new PhotoDTO
            {
                Id = photoId,
                Title = "Cassy",
                PhotoFile = null,
                ContestId = 1,
                UserId = 1
            };

            _photoRepository.Setup(x => x.GetById(It.IsAny<int>()))
                .ReturnsAsync(photoDto);

            //Act
            PhotoDTO photo = await _sut.GetById(photoId);

            //Assert
            Assert.AreEqual(expected: photoId, actual: photo.Id);
            Assert.AreEqual(expected: photoDto.Title, actual: photo.Title);
            Assert.AreEqual(expected: photoDto.PhotoFile, actual: photo.PhotoFile);

        }

        [TestMethod]
        public async Task GetById_ShouldReturnNothing_WhenPhotoDoesNotExist()
        {
            //Arrange
            var photoId = 1;

            _photoRepository.Setup(x => x.GetById(It.IsAny<int>()))
                .ReturnsAsync(() => null);

            //Act
            PhotoDTO photo = await _sut.GetById(photoId);

            //Asserthttps://www.msn.com/en-us/feed
            Assert.IsNull(photo);
        }

        [TestMethod]
        public async Task GetById_ShouldReturnOnce_WhenPhotoExists()
        {
            //Arrange
            var photoId = 1;
            var photoDto = new PhotoDTO
            {
                Id = photoId,
                Title = "Cassy",
                PhotoFile = null,
                ContestId = 1,
                UserId = 1
            };

            _photoRepository.Setup(x => x.GetById(It.IsAny<int>()))
                .ReturnsAsync(photoDto);

            //Act
            PhotoDTO photo = await _sut.GetById(photoId);

            //Assert
            _photoRepository.Verify(x => x.GetById(photoId), Times.Once);
        }
    }
}
