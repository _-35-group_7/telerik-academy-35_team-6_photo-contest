using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using PhotoContest.Domain.Contracts.Repositories;
using PhotoContest.Domain.Contracts.Services;
using PhotoContest.Domain.DataTransferObjects;
using PhotoContest.Domain.Enums;
using PhotoContest.Services;
using System;
using System.Threading.Tasks;

namespace PhotoContest.Tests.Services.ContestServiceTests
{
    [TestClass]
    public class GetById_Should
    {
        
        private readonly ContestService _sut;
        private readonly Mock<IContestRepository> _contestRepository = new Mock<IContestRepository>();
        private readonly Mock<IUserService> _userService = new Mock<IUserService>();
        public GetById_Should()
        {
            _sut = new ContestService(_contestRepository.Object, _userService.Object);
        }

        [TestMethod]
        public async Task GetById_ShouldReturnContest_WhenContestExists()
        {
            //Arrange
            var contestId = 1;
            var contestDto = new ContestDTO
            {
                Id = contestId,
                Title = "First Contest",
                Category = "Dog pictures",
                Type = TypeContest.Open,
                Phase = Stage.PhaseI,
                PhaseITerm = DateTime.Now,
                PhaseIITerm = DateTime.Now
            };

            _contestRepository.Setup(x => x.GetById(It.IsAny<int>()))
                .ReturnsAsync(contestDto);

            //Act
            ContestDTO contest = await _sut.GetById(contestId);

            //Assert
            Assert.AreEqual(expected: contestId, actual: contest.Id);
            Assert.AreEqual(expected: contestDto.Title, actual: contest.Title);
            Assert.AreEqual(expected: contestDto.Category, actual: contest.Category);
            Assert.AreEqual(expected: contestDto.Type, actual: contest.Type);
            Assert.AreEqual(expected: contestDto.Phase, actual: contest.Phase);

        }

        [TestMethod]
        public async Task GetById_ShouldReturnNothing_WhenContestDoesNotExist()
        {
            //Arrange
            var contestId = 1;

            _contestRepository.Setup(x => x.GetById(It.IsAny<int>()))
                .ReturnsAsync(() => null);

            //Act
            ContestDTO contest = await _sut.GetById(contestId);

            //Asserthttps://www.msn.com/en-us/feed
            Assert.IsNull(contest);
        }

        [TestMethod]
        public async Task GetById_ShouldReturnOnce_WhenContestExists()
        {
            //Arrange
            var contestId = 1;
            var contestDto = new ContestDTO
            {
                Id = contestId,
                Title = "First Contest",
                Category = "Dog pictures",
                Type = TypeContest.Open,
                Phase = Stage.PhaseI,
                PhaseITerm = DateTime.Now,
                PhaseIITerm = DateTime.Now
            };

            _contestRepository.Setup(x => x.GetById(It.IsAny<int>()))
                .ReturnsAsync(contestDto);

            //Act
            ContestDTO contest = await _sut.GetById(contestId);

            //Assert
            _contestRepository.Verify(x => x.GetById(contestId), Times.Once);
        }
    }
}
