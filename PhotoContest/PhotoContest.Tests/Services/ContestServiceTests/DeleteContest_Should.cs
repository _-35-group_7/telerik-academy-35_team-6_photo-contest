﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using PhotoContest.Data;
using PhotoContest.Data.Entities;
using PhotoContest.Domain.Contracts.Repositories;
using PhotoContest.Domain.Contracts.Services;
using PhotoContest.Domain.DataTransferObjects;
using PhotoContest.Domain.Enums;
using PhotoContest.Services;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace PhotoContest.Tests.Services.ContestServiceTests
{
    [TestClass]
    public class DeleteContest_Should
    {
        private readonly Mock<IContestRepository> _contestRepository = new Mock<IContestRepository>();
        private readonly ContestService _sut;
        private readonly Mock<IUserService> _userservice = new Mock<IUserService>();

        public DeleteContest_Should()
        {
            _sut = new ContestService(_contestRepository.Object,_userservice.Object);

        }

        [TestMethod]
        public async Task DeleteContest_When_ParamsAreValid()
        {
            //Arrange
            var contestId = 1;
            var contestDto = new ContestDTO
            {
                Id = contestId,
                Title = "First Contest",
                Category = "Dog pictures",
                Type = TypeContest.Open,
                Phase = Stage.PhaseI,
                PhaseITerm = DateTime.Now,
                PhaseIITerm = DateTime.Now
            };

            _contestRepository.Setup(x => x.Delete(contestId))
                .Returns(Task.FromResult(true));

            //Act
            bool contestDeleted = await _sut.Delete(contestDto.Id);

            //Assert
            Assert.AreEqual(expected: true, actual: contestDeleted);
        }

        [TestMethod]
        public async Task ThrowEntityNotFoundException_When_ContestNotFound()
        {
            // Arrange
            var contestId = 1;
            var contestDto = new ContestDTO
            {
                Id = contestId,
                Title = "First Contest",
                Category = "Dog pictures",
                Type = TypeContest.Open,
                Phase = Stage.PhaseI,
                PhaseITerm = DateTime.Now,
                PhaseIITerm = DateTime.Now
            };

            _contestRepository.Setup(x => x.Delete(int.MaxValue))
                .Returns(Task.FromResult(false));

            //Act
            bool contestDeleted = await _sut.Delete(contestDto.Id);

            //Assert
            Assert.AreEqual(expected: false, actual: contestDeleted);
        }

    }
}
