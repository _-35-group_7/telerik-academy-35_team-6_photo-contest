﻿using System;

namespace PhotoContest.Domain.Validation
{
    [AttributeUsage(AttributeTargets.Property)]
    public class DefaultSortAttribute : Attribute { }
}
