﻿
namespace PhotoContest.Domain.Resources
{
    public static class Numbers
    {
        public const int MaxPageSize = 20;
        public const int DefaultPageSize = 10;
        public const int DefaultPage = 1;

    }
}
