﻿namespace PhotoContest.Domain.Resources
{
    public static class Strings
    {
        public const string InvalidSortingProperty =
            "Invalid property for sorting:\nProperty {0} does not exist or can't be sorted.";

    }
}
