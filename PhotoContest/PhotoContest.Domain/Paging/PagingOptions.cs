﻿using PhotoContest.Domain.Contracts.Paging;
using PhotoContest.Domain.Resources;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhotoContest.Domain.Paging
{
    public class PagingOptions : IPagingOptions
    {

        private const int MaxPageSize = Numbers.MaxPageSize;
        private int _pageSize = Numbers.DefaultPageSize;

        public int PageNumber { get; set; } = Numbers.DefaultPage;

        public string SortByProperty { get; set; }

        public bool SortInDescendingOrder { get; set; } = false;

        public int PageSize
        {
            get => _pageSize;
            set => _pageSize = (value > MaxPageSize) ? MaxPageSize : value;
        }
    }
}
