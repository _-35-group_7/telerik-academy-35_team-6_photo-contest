﻿using PhotoContest.Domain.DataTransferObjects;
using PhotoContest.Domain.Enums;
using PhotoContest.Domain.Validation;
using PhotoContest.Domain.ViewModels.PhotoViewModels;
using PhotoContest.Domain.ViewModels.UserViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhotoContest.Domain.ViewModels.ContestViewModels
{
    public class PaginatedContestGetViewModel
    {
        public int Id { get; set; }

        [Sortable]
        public string Title { get; set; }

        [Sortable]
        public string Category { get; set; }

        [Sortable]
        public TypeContest Type { get; set; }

        [Sortable]
        public Stage Phase { get; set; }

        public DateTime PhaseITerm { get; set; }

        public DateTime PhaseIITerm { get; set; }

        public ICollection<PhotoGetViewModel> Photos { get; set; } = new List<PhotoGetViewModel>();
        
        public ICollection<UserGetViewModel> Partakers { get; set; } = new List<UserGetViewModel>();
    }
}
