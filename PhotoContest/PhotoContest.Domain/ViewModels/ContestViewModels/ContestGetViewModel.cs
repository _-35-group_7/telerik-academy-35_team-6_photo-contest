﻿using PhotoContest.Domain.Enums;
using PhotoContest.Domain.Validation;
using PhotoContest.Domain.ViewModels.PhotoViewModels;
using PhotoContest.Domain.ViewModels.UserViewModels;
using System;
using System.Collections.Generic;


namespace PhotoContest.Domain.ViewModels.ContestViewModels
{
    public class ContestGetViewModel
    {
        public int Id { get; set; }
        public string Title { get; set; }

        public string Category { get; set; }

        public TypeContest Type { get; set; }

        public Stage Phase { get; set; }

        public DateTime PhaseITerm { get; set; }

        public DateTime PhaseIITerm { get; set; }

        public List<PhotoGetViewModel> Photos { get; set; } = new List<PhotoGetViewModel>();

        public List<UserGetViewModel> Jury { get; set; } = new List<UserGetViewModel>();

        public List<UserGetViewModel> Partakers { get; set; } = new List<UserGetViewModel>();
    }

}
