﻿using PhotoContest.Domain.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PhotoContest.Domain.ViewModels.ContestViewModels
{
    public class ContestPutViewModel
    {
        public string Title { get; set; }

        public string Category { get; set; }

        public TypeContest Type { get; set; }

        public DateTime PhaseITerm { get; set; }

        public DateTime PhaseIITerm { get; set; }
    }
}
