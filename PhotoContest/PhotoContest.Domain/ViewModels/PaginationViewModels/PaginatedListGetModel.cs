﻿using System.Collections.Generic;

namespace PhotoContest.Api.ViewModels.PaginationViewModels
{
    public class PaginatedListGetModel<T>
    {
        public IEnumerable<T> Elements { get; set; }

        public int CurrentPage { get; set; }

        public int NumOfTotalPages { get; set; }
    }
}
