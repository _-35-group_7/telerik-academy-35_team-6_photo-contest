﻿using PhotoContest.Domain.Enums;
using PhotoContest.Domain.Validation;

namespace PhotoContest.Domain.ViewModels.UserViewModels
{
    public class UserGetViewModel
    {
        public int Id { get; set; }
        [Sortable]
        public string FirstName { get; set; }

        [Sortable]
        public string LastName { get; set; }

        [Sortable]
        public string Username { get; set; }

        public Role Role { get; set; }

        public int Ranking { get; set; }
    }
}
