﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhotoContest.Domain.ViewModels.RatingViewModels
{
    public class RatingGetViewModel
    {
        public int Score { get; set; }

        public string Comment { get; set; }

        public string ContestTitle { get; set; }

        public string PhotoTitle { get; set; }
    }
}
