﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhotoContest.Domain.ViewModels.RatingViewModels
{
    public class RatingPostViewModel
    {
        public int Score { get; set; }

        public string Comment { get; set; }

        public bool IsReviewed { get; set; } = true;

        public int PhotoId { get; set; }
    }
}
