﻿using Microsoft.AspNetCore.Http;
using PhotoContest.Domain.DataTransferObjects;
using PhotoContest.Domain.Validation;


namespace PhotoContest.Domain.ViewModels.PhotoViewModels
{
    public class PhotoGetViewModel
    {
        public int Id { get; set; }

        [Sortable]
        public string Title { get; set; }

        public IFormFile PhotoFile { get; set; }

        public string PhotoUrl { get; set; }

        public string Story { get; set; }

        public double Scoring { get; set; }

        public string Username { get; set; }

        [Sortable]
        public string ContestTitle { get; set; }

        public ContestDTO Contest { get; set; }
    }
}
