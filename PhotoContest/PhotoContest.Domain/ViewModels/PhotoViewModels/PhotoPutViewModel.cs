﻿

using Microsoft.AspNetCore.Http;

namespace PhotoContest.Domain.ViewModels.PhotoViewModels
{
    public class PhotoPutViewModel
    {
        public string Title { get; set; }

        public IFormFile PhotoFile { get; set; }

        public string PhotoUrl { get; set; }
    }
}
