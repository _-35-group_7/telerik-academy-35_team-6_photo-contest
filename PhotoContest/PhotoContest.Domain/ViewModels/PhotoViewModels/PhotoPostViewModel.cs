﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PhotoContest.Domain.ViewModels.PhotoViewModels
{
    public class PhotoPostViewModel
    {
        public string Title { get; set; }

        public IFormFile PhotoFile { get; set; }

        public string PhotoUrl { get; set; }

        public string Story { get; set; }

        public int UserId { get; set; }

        public int ContestId { get; set; }
       
    }
}
