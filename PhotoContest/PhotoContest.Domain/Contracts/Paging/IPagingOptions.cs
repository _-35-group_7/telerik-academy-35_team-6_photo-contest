﻿
namespace PhotoContest.Domain.Contracts.Paging
{
    public interface IPagingOptions
    {
        int PageNumber { get; set; }

        int PageSize { get; set; }
    }
}
