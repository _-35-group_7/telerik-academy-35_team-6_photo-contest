﻿using PhotoContest.Domain.DataTransferObjects;
using PhotoContest.Domain.Paging;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace PhotoContest.Domain.Contracts.Repositories
{
    public interface IUserRepository : IBaseRepository<UserDTO>

    {
        Task<UserDTO> GetByUsername(string username);

        Task<PaginatedList<UserDTO>> GetPaginated(PagingOptions options);

        Task DuplicateEmailAsync(string email);

        Task DuplicateUsernameAsync(string username);
    }
}
