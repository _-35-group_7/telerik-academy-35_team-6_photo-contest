﻿using PhotoContest.Domain.DataTransferObjects;
using PhotoContest.Domain.Paging;
using System;
using System.Threading.Tasks;

namespace PhotoContest.Domain.Contracts.Repositories
{
    public interface IBaseRepository<TDto>
    {
        Task<TDto> GetById(int id);

        Task<TDto> Create(TDto dto);

        Task<TDto> Update(TDto dto);

        Task<bool> Delete(int id);
    }
}
