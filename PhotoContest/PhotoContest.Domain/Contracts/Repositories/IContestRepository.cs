﻿using PhotoContest.Domain.DataTransferObjects;
using PhotoContest.Domain.Paging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhotoContest.Domain.Contracts.Repositories
{
    public interface IContestRepository : IBaseRepository<ContestDTO>
    {
        Task<PaginatedList<ContestDTO>> GetPaginated(PagingOptions options);
    }
}
