﻿using PhotoContest.Domain.DataTransferObjects;
using PhotoContest.Domain.ViewModels.RatingViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhotoContest.Domain.Contracts.Repositories
{
    public interface IRatingRepository : IBaseRepository<RatingDTO>
    {
        Task<RatingDTO> Create(RatingDTO dto);

        Task<RatingDTO> GetById(int id);
    }
}
