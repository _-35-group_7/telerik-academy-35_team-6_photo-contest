﻿using PhotoContest.Domain.DataTransferObjects;
using PhotoContest.Domain.Paging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhotoContest.Domain.Contracts.Repositories
{
    public interface IPhotoRepository:IBaseRepository<PhotoDTO>
    {
        Task<PaginatedList<PhotoDTO>> GetPaginated(PagingOptions options);

        void AddScore(int score, PhotoDTO photoDTO);
    }
}
