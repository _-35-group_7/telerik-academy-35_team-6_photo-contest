﻿using PhotoContest.Domain.Contracts.Repositories;
using PhotoContest.Domain.DataTransferObjects;
using PhotoContest.Domain.Paging;
using PhotoContest.Domain.ViewModels.PhotoViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhotoContest.Domain.Contracts.Services
{
    public interface IContestService : IBaseService<ContestDTO, IContestRepository>
    {
        Task<PaginatedList<ContestDTO>> GetPaginated(PagingOptions options);

        Task<bool> CheckIfParticipantInContestAsync(string username, int contestId);

    }
}
