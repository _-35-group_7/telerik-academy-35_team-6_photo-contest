﻿using PhotoContest.Domain.DataTransferObjects;
using PhotoContest.Domain.ViewModels.RatingViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhotoContest.Domain.Contracts.Services
{
    public interface IRatingService : IBaseService<RatingDTO, IRatingService>
    {
        Task<RatingDTO> Create(RatingDTO dto);
    }
    
}
