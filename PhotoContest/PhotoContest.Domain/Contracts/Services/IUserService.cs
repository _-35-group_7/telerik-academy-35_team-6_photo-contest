﻿using PhotoContest.Domain.Contracts.Repositories;
using PhotoContest.Domain.DataTransferObjects;
using PhotoContest.Domain.Paging;
using System;
using System.Threading.Tasks;

namespace PhotoContest.Domain.Contracts.Services
{
    public interface IUserService : IBaseService<UserDTO, IUserRepository>
    {
        Task<UserDTO> GetByUsername(string username);

        Task<PaginatedList<UserDTO>> GetPaginated(PagingOptions options);
    }
}
