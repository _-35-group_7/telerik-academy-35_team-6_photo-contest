﻿using PhotoContest.Domain.DataTransferObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhotoContest.Domain.Contracts.Services
{
    public interface IAccountService
    {
        Task Register(UserDTO user, string password);

        string GenerateJwtToken(UserDTO user);

        UserDTO Login(string username, string password);
    }
}
