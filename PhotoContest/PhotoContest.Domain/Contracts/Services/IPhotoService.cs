﻿using PhotoContest.Domain.Contracts.Repositories;
using PhotoContest.Domain.DataTransferObjects;
using PhotoContest.Domain.Paging;
using PhotoContest.Domain.ViewModels.PhotoViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhotoContest.Domain.Contracts.Services
{
    public interface IPhotoService : IBaseService<PhotoDTO,IPhotoRepository>
    {
        Task<PhotoGetViewModel> Create(PhotoPostViewModel dto);

        Task<PaginatedList<PhotoDTO>> GetPaginated(PagingOptions options);
        Task<List<PhotoGetViewModel>> GetPhotosForContest(int contestId);

        void AddScore(int score, PhotoDTO photoDTO);
    }
}
