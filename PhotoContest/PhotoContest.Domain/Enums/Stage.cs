﻿
namespace PhotoContest.Domain.Enums
{
    public enum Stage
    {
        PhaseI = 1,
        PhaseII = 2,
        Finished = 3
    }
}
