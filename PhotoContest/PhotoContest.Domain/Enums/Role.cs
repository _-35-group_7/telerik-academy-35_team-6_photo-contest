﻿
namespace PhotoContest.Domain.Enums
{

    public enum Role
    {
        Organizer = 1,
        PhotoJunkie = 2,
        Enthusiast = 3,
        Master = 4,
        Dictator = 5
    }
}
