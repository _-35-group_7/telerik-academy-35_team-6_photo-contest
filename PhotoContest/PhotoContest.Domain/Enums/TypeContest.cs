﻿namespace PhotoContest.Domain.Enums
{
    public enum TypeContest
    {
        Open = 1,
        Invitational = 2
    }
}
