﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhotoContest.Domain.DataTransferObjects
{
    public class RatingDTO : BaseDTO
    {
        public int Score { get; set; }
        public string Comment { get; set; }
        public bool IsReviewed { get; set; } = true;

        public int PhotoId { get; set; }
        public PhotoDTO Photo { get; set; }
    }
}
