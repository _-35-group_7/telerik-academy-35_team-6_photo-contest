﻿

using Microsoft.AspNetCore.Http;

namespace PhotoContest.Domain.DataTransferObjects
{
    public class PhotoDTO : BaseDTO
    {
        public string Title { get; set; }

        public IFormFile PhotoFile { get; set; }

        public string PhotoUrl { get; set; }

        public string Story { get; set; }

        public double Scoring { get; set; }

        public int UserId { get; set; }

        public UserDTO User { get; set; }

        public int ContestId { get; set; }

        public ContestDTO Contest { get; set; }
    }
}
