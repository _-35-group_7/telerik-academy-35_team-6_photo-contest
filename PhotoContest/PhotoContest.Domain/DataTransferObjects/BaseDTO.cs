﻿using System;

namespace PhotoContest.Domain.DataTransferObjects
{
    public class BaseDTO
    {
        public int Id { get; set; }

        public DateTime CreatedOn { get; set; }
    }

}
