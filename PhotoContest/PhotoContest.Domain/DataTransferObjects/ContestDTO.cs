﻿using PhotoContest.Domain.Enums;
using PhotoContest.Domain.ViewModels.PhotoViewModels;
using System;
using System.Collections.Generic;

namespace PhotoContest.Domain.DataTransferObjects
{
    public class ContestDTO : BaseDTO
    {
        public string Title { get; set; }

        public string Category { get; set; }

        public TypeContest Type { get; set; } = TypeContest.Open;

        public Stage Phase { get; set; } = Stage.PhaseI;

        public DateTime PhaseITerm { get; set; }

        public DateTime PhaseIITerm { get; set; }

        public ICollection<PhotoDTO> Photos { get; set; } = new List<PhotoDTO>();

        public ICollection<UserDTO> Jury { get; set; } = new List<UserDTO>();

        public ICollection<UserDTO> Partakers { get; set; } = new List<UserDTO>();
    }
}
