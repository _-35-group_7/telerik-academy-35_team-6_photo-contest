﻿using PhotoContest.Domain.Enums;
using System.ComponentModel.DataAnnotations;

namespace PhotoContest.Domain.DataTransferObjects
{
    public class UserDTO : BaseDTO
    {
        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Username { get; set; }

        public string Password { get; set; }

        public string Email { get; set; }

        public Role Role { get; set; } = Role.PhotoJunkie;

        public int Ranking { get; set; } = 0;

        [Required]
        public string PasswordHash { get; set; }
    }
}
