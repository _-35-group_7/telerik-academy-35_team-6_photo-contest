﻿using System.Collections.Generic;

namespace PhotoContest.Domain.DataTransferObjects
{
    public class PaginationDTO<TDto>
                                where TDto : BaseDTO
    {
        public List<TDto> Elements { get; set; } = new List<TDto>();

        public int Pages { get; set; }

        public int CurrentPage { get; set; }
    }
}
