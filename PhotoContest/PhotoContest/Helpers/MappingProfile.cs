﻿using AutoMapper;
using PhotoContest.Domain.ViewModels;
using PhotoContest.Domain.ViewModels.ContestViewModels;
using PhotoContest.Domain.ViewModels.PhotoViewModels;
using PhotoContest.Data.Entities;
using PhotoContest.Domain.DataTransferObjects;
using PhotoContest.Domain.Paging;
using PhotoContest.Api.ViewModels.PaginationViewModels;
using PhotoContest.Domain.ViewModels.UserViewModels;
using PhotoContest.Domain.ViewModels.RatingViewModels;

namespace PhotoContest.Api.Helpers
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            //Paging
            CreateMap<PagingOptionsModel, PagingOptions>()
                .ForMember(p => p.PageNumber, opt => opt.MapFrom(m => m.Page))
                .ForMember(target => target.SortByProperty, source => source.MapFrom(dto => dto.SortBy));

            //Users pagination
            CreateMap<PaginatedList<UserDTO>, PaginatedListGetModel<UserGetViewModel>>()
                .ForMember(p => p.NumOfTotalPages, opt => opt.MapFrom(po => po.TotalPages));
            CreateMap<PaginatedList<User>, PaginatedList<UserDTO>>();

            //Contests pagination
            CreateMap<PaginatedList<ContestDTO>, PaginatedListGetModel<PaginatedContestGetViewModel>>()
                .ForMember(p => p.NumOfTotalPages, opt => opt.MapFrom(po => po.TotalPages));
            CreateMap<PaginatedList<Contest>, PaginatedList<ContestDTO>>();

            //Photos pagination
            CreateMap<PaginatedList<PhotoDTO>, PaginatedListGetModel<PhotoGetViewModel>>()
                .ForMember(p => p.NumOfTotalPages, opt => opt.MapFrom(po => po.TotalPages));
            CreateMap<PaginatedList<Photo>, PaginatedList<PhotoDTO>>();

            //Admins
            CreateMap<AdminDTO, Admin>().ReverseMap();

            //Users
            CreateMap<UserPostViewModel, UserDTO>();
            CreateMap<UserPutViewModel, UserDTO>().ReverseMap();
            CreateMap<UserDTO, UserGetViewModel>();
            CreateMap<UserDTO, User>().ReverseMap();
            CreateMap<UserDTO, UserRegPostModel>().ReverseMap();
            CreateMap<User, PaginationDTO<UserDTO>>().ReverseMap();

            //Contests
            CreateMap<ContestPostViewModel, ContestDTO>();
            CreateMap<ContestPutViewModel, ContestDTO>().ReverseMap();
            CreateMap<ContestDTO, PaginatedContestGetViewModel>()
                .ForMember(p => p.Id, opt => opt.MapFrom(m => m.Id)).ReverseMap();
            CreateMap<ContestDTO, ContestGetViewModel>().ReverseMap();
            CreateMap<ContestDTO, Contest>().ReverseMap();
            CreateMap<Contest, PaginationDTO<ContestDTO>>().ReverseMap();
            CreateMap<Contest, ContestGetViewModel>().ReverseMap();
            CreateMap<Contest, PaginatedContestGetViewModel>().ReverseMap();

            //Photos
            CreateMap<PhotoPostViewModel, PhotoDTO>().ReverseMap();
            CreateMap<PhotoPostViewModel, PhotoGetViewModel>().ReverseMap();
            CreateMap<PhotoPutViewModel, PhotoDTO>().ReverseMap();
            CreateMap<PhotoDTO, PhotoGetViewModel>()
                .ForMember(p => p.Username, opt => opt.MapFrom(ph => ph.User.Username))
                .ReverseMap();
            CreateMap<PhotoDTO, Photo>().ReverseMap();
            CreateMap<Photo, PaginationDTO<PhotoDTO>>().ReverseMap();

            //Ratings
            CreateMap<RatingPostViewModel, RatingDTO>().ReverseMap();
            CreateMap<Rating, RatingDTO>().ReverseMap();
            CreateMap<RatingDTO, RatingGetViewModel>()
                .ReverseMap();
            

        }
    }
}
