﻿using PhotoContest.Data.Entities;
using PhotoContest.Data.Exceptions;
using PhotoContest.Domain.Contracts.Services;
using PhotoContest.Domain.DataTransferObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PhotoContest.Api.Helpers
{
    public class AuthorizationHelper
    {
        private readonly IUserService userService;

        public AuthorizationHelper(IUserService usersService)
        {
            this.userService = usersService;
        }

        public Task<UserDTO> TryGetUser(string username)
        {
            try
            {
                return userService.GetByUsername(username);
            }
            catch (EntityNotFoundException)
            {
                throw new UnauthorizedOperationException("Invalid Username");
            }
        }
        public Task<UserDTO> TryGetUser(string username, string password)
        {
            var user = this.TryGetUser(username);
            if (user.Result.Password != password)
            {
                throw new UnauthorizedOperationException("Invalid credentials.");
            }
            return user;
        }
    }
}
