﻿using PhotoContest.Domain.Resources;
using System;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace PhotoContest.Api.Helpers
{
    public class PagingOptionsModel
    {

        [Range(1, int.MaxValue, ErrorMessage = "Please enter a value bigger than 0.")]
        public int Page { get; set; } = Numbers.DefaultPage;

        [Range(1, int.MaxValue, ErrorMessage = "Please enter a value bigger than 0.")]
        public int PageSize { get; set; } = Numbers.DefaultPageSize;

        public string SortBy { get; set; } = "Title";

        public bool SortInDescendingOrder { get; set; } = false;

        
    }
}
