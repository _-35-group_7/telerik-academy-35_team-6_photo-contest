﻿using AutoMapper;
using FluentValidation.Results;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using PhotoContest.Api.HelperClasses;
using PhotoContest.Api.Helpers;
using PhotoContest.Api.ViewModels.PaginationViewModels;
using PhotoContest.Data.Validators;
using PhotoContest.Domain.Contracts.Services;
using PhotoContest.Domain.DataTransferObjects;
using PhotoContest.Domain.Paging;
using PhotoContest.Domain.Validation;
using PhotoContest.Domain.ViewModels.ContestViewModels;
using PhotoContest.Domain.ViewModels.PhotoViewModels;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PhotoContest.Api.Controllers
{
    public class ContestsController : BaseController
    {
        private readonly IContestService _contestService;
        private readonly IUserService _userService;
        private readonly IPhotoService _photoService;

        public ContestsController(IContestService contestService, IPhotoService photoService, IUserService userService, IMapper mapper) : base(mapper)
        {
            _contestService = contestService;
            _photoService = photoService;
            _userService = userService;
        }

        public async Task<IActionResult> Index(PagingOptionsModel optionsModel)
        {
            if (!ValidateSortingProperty<PaginatedContestGetViewModel>(optionsModel))
            {
                this.ModelState.AddModelError("Title",
                    string.Format("Invalid property for sorting:\nProperty {0} does not exist or can't be sorted."
                    , optionsModel.SortBy));
                return this.View(optionsModel);
            }
            var currentUserUsername = HttpContext.Session.GetString("CurrentUser");
            var user = await _userService.GetByUsername(currentUserUsername);
            if (user != null)
            {
                bool role = false;
                if (user.Role == Domain.Enums.Role.Organizer)
                {
                    role = true;
                }
                TempData["currentUserRole"] = role;
            }
            else TempData["currentUserRole"] = false;
            var options = _mapper.Map<PagingOptions>(optionsModel);

            var paginatedContests = _mapper.Map<PaginatedListGetModel<PaginatedContestGetViewModel>>(await _contestService.GetPaginated(options));

            return View(paginatedContests);
        }

        public async Task<IActionResult> Open(int contestId)
        {            
            var contestDto = await _contestService.GetById(contestId);

            if(contestDto.PhaseITerm< System.DateTime.Now && contestDto.Phase == Domain.Enums.Stage.PhaseI )
            {
                contestDto.Phase = Domain.Enums.Stage.PhaseII;
                await _contestService.Update(contestDto);
            }

            if (contestDto.PhaseIITerm < System.DateTime.Now)
            {
                contestDto.Phase = Domain.Enums.Stage.Finished;
                await _contestService.Update(contestDto);
            }


            var currentUserUsername = HttpContext.Session.GetString("CurrentUser");
            var user = await _userService.GetByUsername(currentUserUsername);
            if (user != null)
            {
                bool role = false;
                if (user.Role == Domain.Enums.Role.Organizer)
                {
                    role = true;
                }
                TempData["currentUserRole"] = role;
            }
            else TempData["currentUserRole"] = false;
            TempData["AlreadyParticipate"] = false;//await _contestService.CheckIfParticipantInContestAsync(currentUserUsername, contestId); todo

            var contestViewModel = _mapper.Map<ContestGetViewModel>(contestDto);

            return View(contestViewModel);
        }
        public IActionResult Create()
        {
            ContestPostViewModel contestViewModel = new();
            return View(contestViewModel);
        }
        [HttpPost]
        public async Task<IActionResult> Create(ContestPostViewModel model)
        {
            model.Phase = Domain.Enums.Stage.PhaseI;
            ContestValidator _validator = new ContestValidator();
            ValidationResult validResult = _validator.Validate(model);

            if (!validResult.IsValid)
            {
                return BadRequest(validResult.Errors);
            }


            var input = _mapper.Map<ContestDTO>(model);
            await _contestService.Create(input);


            return RedirectToAction("Index", "Contests");
        }
        [HttpPost]
        public async Task<IActionResult> FinishContest(int contestId)
        {
            var contestDto = await _contestService.GetById(contestId);
            var scoring = new List<PhotoDTO>();
            foreach (var item in contestDto.Photos)
            {
                scoring.Add(item);
            }
            var topThree=scoring.OrderByDescending(x => x.Scoring).Take(3).ToList();


            for (int i = 0; i < topThree.Count; i++)
            {
                UserDTO user =await _userService.GetById(topThree[i].User.Id);
                if (i == 0)
                {
                    user.Ranking += 50;
                    await _userService.Update(user);
                }
                if (i == 1)
                {
                    user.Ranking += 35;
                    await _userService.Update(user);
                }
                if (i == 2)
                {
                    user.Ranking += 25;
                   await  _userService.Update(user);
                }

            }
            return RedirectToAction("Index", "Contests");
        }


    }
}

