﻿using FluentValidation.Results;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using PhotoContest.Api.Helpers;
using PhotoContest.Data.Entities;
using PhotoContest.Data.Exceptions;
using PhotoContest.Data.Front_endViewModels.AuthModels;
using PhotoContest.Data.Validators;
using PhotoContest.Domain.Contracts.Services;
using PhotoContest.Domain.DataTransferObjects;
using PhotoContest.Services.HelperClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PhotoContest.Api.Controllers
{
    public class AuthController : Controller
    {
        private readonly IAccountService _accountService;
        private readonly IUserService _userService;


        public AuthController(IAccountService accountService, IUserService userService)
        {
             _accountService = accountService;
            _userService = userService;
    }
        public IActionResult Login()
        {
            var loginViewModel = new LoginViewModel();
            return View(loginViewModel);
        }
        [HttpPost]
        public  IActionResult Login([Bind("Username, Password")] LoginViewModel loginViewModel)
        {
            if (!ModelState.IsValid)
            {
                return this.View(loginViewModel);
            }
            try
            {
                var user = _accountService.Login(loginViewModel.Username, loginViewModel.Password);
                HttpContext.Session.SetString("CurrentUser", user.Username);

                return this.RedirectToAction("Index", "Home");
            }
            catch (Exception e)
            {
                ModelState.AddModelError("Username", e.Message);
                return this.View(loginViewModel);
            }
        }       
        public IActionResult Logout()
        {
            this.HttpContext.Session.Remove("CurrentUser");
            return this.RedirectToAction("Index", "Home");
        }
        public IActionResult Register()
        {
            var registerViewModel = new RegisterViewModel();
            return this.View(registerViewModel);
        }
        [HttpPost]
        public async Task<IActionResult> Register(RegisterViewModel registerViewModel)
        {
            UserRegisterValidator validator = new UserRegisterValidator();
            ValidationResult validResult = validator.Validate(registerViewModel);

            if (!validResult.IsValid)
            {
                foreach (var error in validResult.Errors)
                {
                    this.ModelState.AddModelError(error.PropertyName, error.ErrorMessage);
                }
                
                return this.View(registerViewModel);
            }

            var entity = await _userService.GetByUsername(registerViewModel.Username);

            if (entity != null)
            {
                this.ModelState.AddModelError("Username", "Username already exists");
                return this.View(registerViewModel);
            }
          
            if (!registerViewModel.Password.Equals(registerViewModel.ConfirmPassword))
            {
                this.ModelState.AddModelError("ConfirmPassword", "Password should match");
                return this.View(registerViewModel);
            }
            UserDTO user = new UserDTO()
            {
                FirstName = registerViewModel.FirstName,
                LastName = registerViewModel.LastName,
                Username = registerViewModel.Username,
                Email = registerViewModel.Email,
                Password = registerViewModel.Password,
            };
            user.PasswordHash = PasswordHasher.HashPassword(user.Password);
            await _userService.Create(user);
            return this.RedirectToAction("Login", "Auth");
            
        }
    }
}
