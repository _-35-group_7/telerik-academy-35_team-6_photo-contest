﻿using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using PhotoContest.Api.Helpers;
using PhotoContest.Api.ViewModels.PaginationViewModels;
using PhotoContest.Domain.Contracts.Services;
using PhotoContest.Domain.Paging;
using PhotoContest.Domain.ViewModels.UserViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PhotoContest.Api.Controllers
{
    public class UsersController : BaseController
    {
        private readonly IUserService _userService;

        public UsersController(IUserService userService, IMapper mapper) : base(mapper)
        {
            _userService = userService;
        }
        public async Task<IActionResult> Index(PagingOptionsModel optionsModel)
        {
            var currentUserUsername = HttpContext.Session.GetString("CurrentUser");
            var user = await _userService.GetByUsername(currentUserUsername);
            if (user != null)
            {
                bool role = false;
                if (user.Role == Domain.Enums.Role.Organizer)
                {
                    role = true;
                }
                TempData["currentUserRole"] = role;
            }
            else TempData["currentUserRole"] = false;
            optionsModel.SortBy = "Username";
            if (!ValidateSortingProperty<UserGetViewModel>(optionsModel))
            {
                this.ModelState.AddModelError("Username",
                    string.Format("Invalid property for sorting:\nProperty {0} does not exist or can't be sorted."
                    , optionsModel.SortBy));
                return this.View(optionsModel);
            }

            var options = _mapper.Map<PagingOptions>(optionsModel);

            var paginatedContests = _mapper.Map<PaginatedListGetModel<UserGetViewModel>>(await _userService.GetPaginated(options));

            return View(paginatedContests);
        }
    }
}
