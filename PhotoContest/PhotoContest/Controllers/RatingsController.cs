﻿using AutoMapper;
using FluentValidation.Results;
using Microsoft.AspNetCore.Mvc;
using PhotoContest.Data.Validators;
using PhotoContest.Domain.Contracts.Services;
using PhotoContest.Domain.DataTransferObjects;
using PhotoContest.Domain.ViewModels.RatingViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PhotoContest.Api.Controllers
{
    public class RatingsController : BaseController
    {
        private readonly IRatingService _service;
        public RatingsController(IRatingService service, IMapper mapper) : base(mapper)
        {
            _service = service;
        }
        public IActionResult Create(int photoId)
        {
            RatingPostViewModel ratingViewModel = new()
            {
                PhotoId = photoId
            };
            return View(ratingViewModel);
        }

        [HttpPost]
        public async Task<IActionResult> Create(RatingPostViewModel model)
        {
            RatingValidator _validator = new RatingValidator();
            ValidationResult validResult = _validator.Validate(model);

            if (!validResult.IsValid)
            {
                return BadRequest(validResult.Errors);
            }

            var input = _mapper.Map<RatingDTO>(model);
            var result = await _service.Create(input);

            return RedirectToAction("Index", "Contests");
        }
    }
}
