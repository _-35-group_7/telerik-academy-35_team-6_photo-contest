﻿using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using PhotoContest.Data.Validators;
using PhotoContest.Domain.Contracts.Services;
using PhotoContest.Domain.DataTransferObjects;
using PhotoContest.Domain.ViewModels.RatingViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using FluentValidation.Results;
using System.Threading.Tasks;

namespace PhotoContest.Api.Controllers
{
    [Route("api/Contests/Ratings")]
    [ApiController]
    public class RatingController : BaseController
    {
        private readonly IRatingService _service;

        public RatingController(IRatingService service, IMapper mapper) : base(mapper)
        {
            _service = service;
        }

        [HttpPost]
        public async Task<IActionResult> Post(RatingPostViewModel model)
        {
            RatingValidator _validator = new RatingValidator();
            ValidationResult validResult = _validator.Validate(model);

            if (!validResult.IsValid)
            {
                return BadRequest(validResult.Errors);
            }

            var input = _mapper.Map<RatingDTO>(model);
            var result = await _service.Create(input);

            return this.StatusCode(StatusCodes.Status200OK, result);
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetById(int id)
        {
            var input = await _service.GetById(id);

            var result = _mapper.Map<RatingGetViewModel>(input);

            return this.StatusCode(StatusCodes.Status200OK, result);
        }

    }
}
