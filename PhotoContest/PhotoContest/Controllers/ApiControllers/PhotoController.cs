﻿using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using PhotoContest.Domain.ViewModels.PhotoViewModels;
using PhotoContest.Data.Exceptions;
using PhotoContest.Domain.Contracts.Services;
using PhotoContest.Domain.DataTransferObjects;
using System;
using System.Threading.Tasks;
using PhotoContest.Domain.Paging;
using PhotoContest.Api.Helpers;
using PhotoContest.Api.ViewModels.PaginationViewModels;
using PhotoContest.Data.Validators;
using FluentValidation.Results;
using Microsoft.AspNetCore.Hosting;
using System.IO;

namespace PhotoContest.Api.Controllers
{
    [Route("api/Photos")]
    [ApiController]
    public class PhotoController: BaseController
    {
        private readonly IPhotoService _service;
        private readonly IWebHostEnvironment _webHostEnvironment;

        public PhotoController(IPhotoService service, IMapper mapper
            ,IWebHostEnvironment webHostEnvironment) : base(mapper)
        {
            _service = service;
            _webHostEnvironment = webHostEnvironment;
        }

        [HttpGet]
        public async Task<IActionResult> GetPaginated([FromQuery] PagingOptionsModel optionsModel)
        {
            if (!ValidateSortingProperty<PhotoGetViewModel>(optionsModel))
            {
                return BadRequest(string.Format("Invalid property for sorting:\nProperty {0} does not exist or can't be sorted."
                    , optionsModel.SortBy));
            }

            var options = _mapper.Map<PagingOptions>(optionsModel);

            var paginatedUsers = _mapper.Map<PaginatedListGetModel<PhotoGetViewModel>>(await _service.GetPaginated(options));

            return this.StatusCode(StatusCodes.Status200OK, paginatedUsers);
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int id)
        {
            var output = await _service.GetById(id);

            var result = _mapper.Map<PhotoGetViewModel>(output);

            return Ok(result);
        }

        [HttpPost]
        public async Task<IActionResult> Post(PhotoPostViewModel model)
        {
            PhotoValidator _validator = new PhotoValidator();
            ValidationResult validResult = _validator.Validate(model);

            if (!validResult.IsValid)
            {
                if (model.PhotoFile != null)
                {
                    string photosFolder = "Photos/";
                    photosFolder += Guid.NewGuid().ToString() + "_" + model.PhotoFile.FileName;

                    model.PhotoUrl = "/" + photosFolder;

                    string serverFolder = Path.Combine(_webHostEnvironment.WebRootPath, photosFolder);

                    await model.PhotoFile.CopyToAsync(new FileStream(serverFolder, FileMode.Create));
                }

                return BadRequest(validResult.Errors);
            }

            var input = _mapper.Map<PhotoDTO>(model);
            var output = await _service.Create(input);

            var result = _mapper.Map<PhotoGetViewModel>(output);

            return Ok(result);
        }

        //[HttpPut("{id}")]
        //public async Task<IActionResult> Put(int id, PhotoPostViewModel model)
        //{
        //    PhotoValidator _validator = new PhotoValidator();
        //    ValidationResult validResult = _validator.Validate(model);
        //
        //    if (!validResult.IsValid)
        //    {
        //        return BadRequest(validResult.Errors);
        //    }
        //
        //    try
        //    {
        //        var input = _mapper.Map<PhotoDTO>(model);
        //        input.Id = id;
        //
        //        var output = await _service.Update(input);
        //        var result = _mapper.Map<PhotoGetViewModel>(output);
        //
        //        return this.StatusCode(StatusCodes.Status200OK, result);
        //    }
        //    catch (EntityNotFoundException e)
        //    {
        //        return this.StatusCode(StatusCodes.Status404NotFound, e.Message);
        //    }
        //    catch (DuplicateEntityException e)
        //    {
        //        return this.StatusCode(StatusCodes.Status409Conflict, e.Message);
        //    }
        //}

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            var result = await _service.Delete(id);

            if (!result)
            {
                return NotFound();
            }

            return Ok();
        }
    }
}
