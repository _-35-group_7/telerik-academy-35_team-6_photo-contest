﻿using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using PhotoContest.Api.Helpers;
using PhotoContest.Domain.ViewModels.ContestViewModels;
using PhotoContest.Api.ViewModels.PaginationViewModels;
using PhotoContest.Data.Exceptions;
using PhotoContest.Domain.Contracts.Services;
using PhotoContest.Domain.DataTransferObjects;
using PhotoContest.Domain.Paging;
using System.Threading.Tasks;
using PhotoContest.Data.Validators;
using FluentValidation.Results;

namespace PhotoContest.Api.Controllers
{
    [Route("api/Contest")]
    [ApiController]
    public class ContestController : BaseController
    {

        private readonly IContestService _service;
        private readonly IUserService _userService;

        public ContestController(IContestService service, IUserService userService, IMapper mapper) : base(mapper)
        {
            _service = service;
            _userService = userService;
        }

        [HttpGet]
        public async Task<IActionResult> GetPaginated([FromQuery] PagingOptionsModel optionsModel)
        {
            if (!ValidateSortingProperty<PaginatedContestGetViewModel>(optionsModel))
            {
                return BadRequest(string.Format("Invalid property for sorting:\nProperty {0} does not exist or can't be sorted."
                    , optionsModel.SortBy));
            }

            var options = _mapper.Map<PagingOptions>(optionsModel);

            var paginatedContests = _mapper.Map<PaginatedListGetModel<PaginatedContestGetViewModel>>(await _service.GetPaginated(options));

            return this.StatusCode(StatusCodes.Status200OK, paginatedContests);
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int id)
        {
            var output = await _service.GetById(id);

            if (output == null)
            {
                return NotFound();
            }

            var result = _mapper.Map<PaginatedContestGetViewModel>(output);

            return this.StatusCode(StatusCodes.Status200OK, result);
        }

        [HttpPost]
        public async Task<IActionResult> Post(ContestPostViewModel model)
        {
            ContestValidator _validator = new ContestValidator();
            ValidationResult validResult = _validator.Validate(model);

            if (!validResult.IsValid)
            {
                return BadRequest(validResult.Errors);
            }


            var input = _mapper.Map<ContestDTO>(model);
            var output = await _service.Create(input);

            var result = _mapper.Map<ContestGetViewModel>(output);

            return this.StatusCode(StatusCodes.Status200OK, result);
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> Put(int id, ContestPostViewModel model)
        {
            ContestValidator _validator = new ContestValidator();
            ValidationResult validResult = _validator.Validate(model);

            if (!validResult.IsValid)
            {
                return BadRequest(validResult.Errors);
            }

            try
            {
                var input = _mapper.Map<ContestDTO>(model);
                input.Id = id;

                var output = await _service.Update(input);
                var result = _mapper.Map<ContestGetViewModel>(output);

                return this.StatusCode(StatusCodes.Status200OK, result);
            }
            catch (EntityNotFoundException e)
            {
                return this.StatusCode(StatusCodes.Status404NotFound, e.Message);
            }
            catch (DuplicateEntityException e)
            {
                return this.StatusCode(StatusCodes.Status409Conflict, e.Message);
            }
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            var result = await _service.Delete(id);

            if (!result)
            {
                return NotFound();
            }

            return Ok();
        }
    }
}
