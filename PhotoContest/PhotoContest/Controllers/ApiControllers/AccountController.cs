﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using PhotoContest.Data.Entities;
using PhotoContest.Data.Validators;
using PhotoContest.Domain.Contracts.Services;
using PhotoContest.Domain.DataTransferObjects;
using PhotoContest.Domain.ViewModels.UserViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PhotoContest.Api.Controllers
{
    public class AccountController : BaseController
    {
        private readonly IAccountService _accountService;
        private IConfiguration _config; 

        public AccountController(IAccountService accountService, IMapper mapper, IConfiguration config)
            : base(mapper)
        {
            _accountService = accountService ?? throw new ArgumentNullException(nameof(accountService));
            _config = config;
        }

        [HttpPost("register")]
        public async Task<IActionResult> Register(UserRegPostModel postModel)
        {
            UserRegisterValidator _validator = new UserRegisterValidator();
            var validResult = _validator.Validate(postModel);

            if (!validResult.IsValid)
            {
                return BadRequest(validResult.Errors);
            }

            var user = _mapper.Map<UserDTO>(postModel);
            await _accountService.Register(user, postModel.Password);

            return Ok();
        }

        [HttpPost("login")]
        public IActionResult Login(UserLogPostModel postModel)
        {
            UserLoginValidator _validator = new UserLoginValidator();
            var validResult = _validator.Validate(postModel);

            if (!validResult.IsValid)
            {
                return BadRequest(validResult.Errors);
            }

            var user = _accountService.Login(postModel.Username, postModel.Password);

            var token = _accountService.GenerateJwtToken(user);

            return Ok(token);

        }
    }
}
