﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using PhotoContest.Api.HelperClasses;
using PhotoContest.Api.Helpers;
using PhotoContest.Domain.Validation;
using System;
using System.Linq;
using System.Reflection;

namespace PhotoContest.Api.Controllers
{
    public abstract class BaseController : Controller
    {
        protected readonly IMapper _mapper;

        protected BaseController(IMapper mapper)
        {
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }

        protected bool ValidateSortingProperty<TOutputModel>(PagingOptionsModel options)
        {
            options.SortBy = options.SortBy.ToPascalCase();
            PropertyInfo result;
            if (options.SortBy != null)
            {
                result = typeof(TOutputModel).GetProperty(options.SortBy);
            }
            else
            {
                result = typeof(TOutputModel).GetProperties().First(property => Attribute.IsDefined(property, typeof(DefaultSortAttribute)));
            }

            if (result == null)
            {
                return false;
            }

            return Attribute.IsDefined(result, typeof(SortableAttribute));
        }
    }
}
