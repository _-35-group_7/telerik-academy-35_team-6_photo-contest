﻿using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using PhotoContest.Api.Helpers;
using PhotoContest.Api.ViewModels;
using PhotoContest.Api.ViewModels.PaginationViewModels;
using PhotoContest.Data;
using PhotoContest.Data.Exceptions;
using PhotoContest.Domain.Contracts.Services;
using PhotoContest.Domain.DataTransferObjects;
using PhotoContest.Domain.ViewModels;
using PhotoContest.Domain.Paging;
using System;
using System.Threading.Tasks;
using PhotoContest.Data.Validators;
using FluentValidation.Results;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using PhotoContest.Domain.Enums;
using PhotoContest.Domain.ViewModels.UserViewModels;

namespace PhotoContest.Api.Controllers
{
    [Route("api/users")]
    [ApiController]
    public class UserController : BaseController
    {

        private readonly IUserService _service;

        public UserController(IUserService service, IMapper mapper) : base(mapper)
        {
            _service = service;
        }

        [HttpGet]
        public async Task<IActionResult> GetPaginated([FromQuery] PagingOptionsModel optionsModel)
        {
            if (!ValidateSortingProperty<UserGetViewModel>(optionsModel))
            {
                return BadRequest(string.Format("Invalid property for sorting:\nProperty {0} does not exist or can't be sorted."
                    , optionsModel.SortBy));
            }

            var options = _mapper.Map<PagingOptions>(optionsModel);

            var paginatedUsers = _mapper.Map<PaginatedListGetModel<UserGetViewModel>>(await _service.GetPaginated(options));

            return this.StatusCode(StatusCodes.Status200OK, paginatedUsers);
        }

        [HttpGet("{id}")]
        [Authorize]
        public async Task<IActionResult> Get(int id)
        {
            var output = await _service.GetById(id);

            if (output == null)
            {
                return NotFound();
            }

            var result = _mapper.Map<UserGetViewModel>(output);

            return Ok(result);
        }

        [HttpPost("")]
        public async Task<IActionResult> Post([FromBody] UserPostViewModel model)
        {
            UserValidator validator = new UserValidator();
            ValidationResult validResult = validator.Validate(model);
            
            if (!validResult.IsValid)
            {
               return BadRequest(validResult.Errors);
            }

            try
            {
                var input = _mapper.Map<UserDTO>(model);
                var output = await _service.Create(input);
                var result = _mapper.Map<UserGetViewModel>(output);
                return this.StatusCode(StatusCodes.Status201Created, result);
            }
            catch (DuplicateEntityException e)
            {
                return this.StatusCode(StatusCodes.Status409Conflict, e.Message);
            }

        }

        [HttpPut("{id}")]
        public async Task<IActionResult> Put(int id, [FromBody] UserPutViewModel model)
        {
            UserPutValidator _validator = new UserPutValidator();
            ValidationResult validResult = _validator.Validate(model);

            if (!validResult.IsValid)
            {
                return BadRequest(validResult.Errors);
            }

            try
            {
                var input = _mapper.Map<UserDTO>(model);
                input.Id = id;

                var output = await _service.Update(input);
                var result = _mapper.Map<UserPutViewModel>(output);

                return this.StatusCode(StatusCodes.Status200OK, result);
            }
            catch (EntityNotFoundException e)
            {
                return this.StatusCode(StatusCodes.Status404NotFound, e.Message);
            }
            catch (DuplicateEntityException e)
            {
                return this.StatusCode(StatusCodes.Status409Conflict, e.Message);
            }
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            var result = await _service.Delete(id);

            if (!result)
            {
                return NotFound();
            }

            return Ok();
        }

    }
}
