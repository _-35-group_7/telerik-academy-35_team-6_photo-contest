﻿using AutoMapper;
using FluentValidation.Results;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using PhotoContest.Api.Helpers;
using PhotoContest.Api.ViewModels.PaginationViewModels;
using PhotoContest.Data.Validators;
using PhotoContest.Domain.Contracts.Services;
using PhotoContest.Domain.DataTransferObjects;
using PhotoContest.Domain.Paging;
using PhotoContest.Domain.ViewModels.PhotoViewModels;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace PhotoContest.Api.Controllers
{
    public class PhotosController : BaseController
    {
        private readonly IContestService _contestService;
        private readonly IUserService _userService;
        private readonly IPhotoService _photoService;
        private readonly IWebHostEnvironment _webHostEnvironment;

        public PhotosController(IContestService contestService, IPhotoService photoService, IMapper mapper,
            IWebHostEnvironment webHostEnvironment, IUserService userService) : base(mapper)
        {
            _contestService = contestService;
            _photoService = photoService;
            _webHostEnvironment = webHostEnvironment;
            _userService = userService;
    }

        public IActionResult Create(int contestId)
        {
            PhotoPostViewModel photoViewModel = new()
            {
                ContestId = contestId
            };
            return View(photoViewModel);
        }
        public IActionResult Index()
        {
            return View();
        }
        public async Task<IActionResult> AllPhotos(int contestId)
        {
            var photosDtos = await _photoService.GetPhotosForContest(contestId);
            var contestDto = await _contestService.GetById(contestId);

            ViewData["contestId"] = contestId;
            TempData["contestTitle"] = contestDto.Title;
            
            return View(photosDtos);
        }


        [HttpPost]
        public async Task<IActionResult> Create(PhotoPostViewModel model, int contestId)
        {
            PhotoValidator _validator = new PhotoValidator();
            ValidationResult validResult = _validator.Validate(model);

            var user = await _userService.GetByUsername(HttpContext.Session.GetString("CurrentUser"));

            if (!validResult.IsValid)
            {
                return BadRequest(validResult.Errors);
            }

            model.UserId = user.Id;
            model.ContestId = contestId;

            string photosFolder = "Photos/";
            photosFolder += Guid.NewGuid().ToString() + "_" + model.PhotoFile.FileName;

            model.PhotoUrl = "/" + photosFolder;

            string serverFolder = Path.Combine(_webHostEnvironment.WebRootPath, photosFolder);

            await model.PhotoFile.CopyToAsync(new FileStream(serverFolder, FileMode.Create));

            await _photoService.Create(model);

            return View(model);
        }
    }
}
