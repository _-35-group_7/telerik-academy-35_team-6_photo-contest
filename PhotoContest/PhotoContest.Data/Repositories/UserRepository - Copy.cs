﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using PhotoContest.Data.Entities;
using PhotoContest.Domain.Contracts.Repositories;
using PhotoContest.Domain.DataTransferObjects;
using PhotoContest.Domain.Paging;
using PhotoContest.Data.Helpers;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using PhotoContest.Data.Validators;
using FluentValidation.Results;

namespace PhotoContest.Data.Repositories
{
    public class UserRepository : BaseRepository<UserDTO, User>, IUserRepository
    {
        public UserRepository(DataContext context, IMapper mapper) : base(context, mapper)
        {

        }


        public async Task<UserDTO> GetByUsername(string username)
        {
            var entity = await _context.Set<User>().AsNoTracking()
                .FirstOrDefaultAsync(x => x.Username == username);

            return _mapper.Map<UserDTO>(entity);
        }

    }
}
