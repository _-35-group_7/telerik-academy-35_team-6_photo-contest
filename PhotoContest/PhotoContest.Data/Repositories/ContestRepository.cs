﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using PhotoContest.Data.Entities;
using PhotoContest.Domain.Contracts.Repositories;
using PhotoContest.Domain.DataTransferObjects;
using PhotoContest.Domain.Paging;
using System;
using System.Collections.Generic;
using System.Linq;
using PhotoContest.Data.Helpers;
using System.Threading.Tasks;
using PhotoContest.Domain.Contracts.Services;
using PhotoContest.Domain.ViewModels.ContestViewModels;

namespace PhotoContest.Data.Repositories
{
    public class ContestRepository : BaseRepository<ContestDTO, Contest>, IContestRepository
    {
        public ContestRepository(DataContext context, IMapper mapper) : base(context, mapper)
        {
        }

        public async Task<PaginatedList<ContestDTO>> GetPaginated(PagingOptions options)
        {
            var query = _context.Contests.OrderBy(options.SortByProperty, options.SortInDescendingOrder);
            var result = await query.ToPaginatedListAsync(options.PageNumber, options.PageSize);

            return _mapper.Map<PaginatedList<ContestDTO>>(result);
        }

        public override async Task<ContestDTO> GetById(int id)
        {
            var entity = await this._context.Contests
                .Include(c => c.Photos)
                    .ThenInclude(photo => photo.User)
                .Include( c => c.Partakers).AsNoTracking()
                .FirstOrDefaultAsync(x => x.Id == id);

            var result = _mapper.Map<ContestDTO>(entity);
            foreach (var item in result.Photos)
            {
                var photo = await _context.Set<Photo>().
                  AsNoTracking().FirstOrDefaultAsync(x => x.Id == item.Id); ;
                item.PhotoFile = photo.PhotoFile;
                item.PhotoUrl = photo.PhotoUrl;
                item.Story = photo.Story;
                item.Scoring = photo.Scoring;
            }
            return result;
        }

    }
}
