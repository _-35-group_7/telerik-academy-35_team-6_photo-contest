﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using PhotoContest.Data.Entities;
using PhotoContest.Data.Exceptions;
using PhotoContest.Domain.Contracts.Repositories;
using PhotoContest.Domain.DataTransferObjects;
using PhotoContest.Domain.Paging;
using System.Linq;
using System.Threading.Tasks;
using PhotoContest.Data.Helpers;
using Microsoft.AspNetCore.Identity;
using PhotoContest.Data.Validators;
using System.ComponentModel.DataAnnotations;

namespace PhotoContest.Data.Repositories
{
    public abstract class BaseRepository<TDto, TEntity> : IBaseRepository<TDto>
        where TDto : BaseDTO
        where TEntity : BaseEntity
    {
        internal readonly DataContext _context;
        internal readonly IMapper _mapper;

        protected BaseRepository(DataContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public virtual async Task<TDto> Create(TDto dto)
        {
            var entity = _mapper.Map<TEntity>(dto);

            await _context.Set<TEntity>().AddAsync(entity);
            await _context.SaveChangesAsync();

            var result = _mapper.Map<TDto>(entity);

            return result;
        }

       public virtual async Task<bool> Delete(int id)
       {
           var entity = await _context.Set<TEntity>().FirstOrDefaultAsync(x => x.Id == id);
       
           if (entity == null)
           {
               return false;
           }
           else
           {
                _context.Remove(entity);
               await _context.SaveChangesAsync();
               return true;
           }
       }

        public virtual async Task<TDto> GetById(int id)
        {
            var entity = await _context.Set<TEntity>().
                AsNoTracking().FirstOrDefaultAsync(x => x.Id == id);

            return _mapper.Map<TDto>(entity);
        }

        public virtual async Task<TDto> Update(TDto dto)
        {
            var entity = _mapper.Map<TEntity>(dto);
            if (entity == null)
            {
                throw new EntityNotFoundException();
            }

            _context.Set<TEntity>().Update(entity);

            await _context.SaveChangesAsync();

            return _mapper.Map<TDto>(entity);
        }
    }
}
