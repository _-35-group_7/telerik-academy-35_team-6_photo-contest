﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using PhotoContest.Data.Entities;
using PhotoContest.Domain.Contracts.Repositories;
using PhotoContest.Domain.DataTransferObjects;
using PhotoContest.Domain.Paging;
using PhotoContest.Data.Helpers;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using PhotoContest.Data.Validators;
using FluentValidation.Results;
using PhotoContest.Data.Exceptions;
using PhotoContest.Domain.Enums;

namespace PhotoContest.Data.Repositories
{
    public class UserRepository : BaseRepository<UserDTO, User>, IUserRepository
    {

        public UserRepository(DataContext context, IMapper mapper) : base(context, mapper)
        {

        }

        public async Task<PaginatedList<UserDTO>> GetPaginated(PagingOptions options)
        {
            var query = _context.Users.OrderBy(options.SortByProperty, options.SortInDescendingOrder);
            var result = await query.ToPaginatedListAsync(options.PageNumber, options.PageSize);

            return _mapper.Map<PaginatedList<UserDTO>>(result);
        }

        public async Task<UserDTO> GetByUsername(string username)
        {
            var entity = await _context.Set<User>()//.AsNoTracking()
                .FirstOrDefaultAsync(x => x.Username == username);

            return _mapper.Map<UserDTO>(entity);
        }

        public override async Task<UserDTO> Create(UserDTO dto)
        {
            var entity = _mapper.Map<User>(dto);
            entity.Password = dto.PasswordHash;

            await _context.Set<User>().AddAsync(entity);
            await _context.SaveChangesAsync();

            var result = _mapper.Map<UserDTO>(entity);

            return result;
        }

        public async Task DuplicateEmailAsync(string email)
        {
            var duplicateEmail = await _context.Users.IgnoreQueryFilters().FirstOrDefaultAsync(ue => ue.Email == email);

            if (duplicateEmail != null)
            {
                throw new DuplicateEntityException("A user with this email already exists!");
            }
        }

        public async Task DuplicateUsernameAsync(string username)
        {
            var duplicateUsername = await _context.Users.IgnoreQueryFilters().FirstOrDefaultAsync(ue => ue.Username == username);

            if (duplicateUsername != null)
            {
                throw new DuplicateEntityException("A user with this username already exists!");
            }
        }
    }
}
