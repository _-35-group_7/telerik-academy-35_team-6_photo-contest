﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using PhotoContest.Data.Entities;
using PhotoContest.Data.Exceptions;
using PhotoContest.Domain.Contracts.Repositories;
using PhotoContest.Domain.Contracts.Services;
using PhotoContest.Domain.DataTransferObjects;
using PhotoContest.Domain.Paging;
using System;
using PhotoContest.Data.Helpers;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhotoContest.Data.Repositories
{
    public class PhotoRepository : BaseRepository<PhotoDTO, Contest>, IPhotoRepository
    {
        public readonly IUserService _userservice;
        public readonly IContestService _contestservice;

        public PhotoRepository(DataContext context, IMapper mapper, IUserService userService,IContestService contestService) : base(context, mapper)
        {
            _userservice = userService;
            _contestservice = contestService;
        }

        public async Task<PaginatedList<PhotoDTO>> GetPaginated(PagingOptions options)
        {
            var query = _context.Photos
                .Include(c => c.Contest)
                .Include(c => c.User)
                .OrderBy(options.SortByProperty, options.SortInDescendingOrder);
            var result = await query.ToPaginatedListAsync(options.PageNumber, options.PageSize);

            return _mapper.Map<PaginatedList<PhotoDTO>>(result);
        }

        public override async Task<PhotoDTO> Create(PhotoDTO dto)
        {
            var user = await _userservice.GetById(dto.UserId);
            var contest = await _contestservice.GetById(dto.ContestId);

            var userEntity =  _mapper.Map<User>(user);
            var contestEntity =  _mapper.Map<Contest>(contest);

            var photoEntity = new Photo()
            {
                Title = dto.Title,
                PhotoFile = dto.PhotoFile,
                ContestId = contestEntity.Id,
                UserId = userEntity.Id,
                PhotoUrl = dto.PhotoUrl,
                Story = dto.Story                
            };
            await _context.Set<Photo>().AddAsync(photoEntity);
            await _context.SaveChangesAsync();


            var result = _mapper.Map<PhotoDTO>(photoEntity);
            result.Contest = contest;
            result.User = user;
            return result;
        }
        public override async Task<PhotoDTO> GetById(int id)
        {

            var entity = await _context.Set<Photo>().
                   AsNoTracking().FirstOrDefaultAsync(x => x.Id == id);

            if (entity == null)
            {
                throw new EntityNotFoundException();
            }

            var dtoContest =await _contestservice.GetById(entity.ContestId);
            var dtoUser =await _userservice.GetById(entity.UserId);

            var userDTO = _mapper.Map<UserDTO>(dtoUser);
            var contestDTO = _mapper.Map<ContestDTO>(dtoContest);

            PhotoDTO dto = new PhotoDTO()
            {
                Title = entity.Title,
                CreatedOn = entity.CreatedOn,
                PhotoFile = entity.PhotoFile,
                PhotoUrl = entity.PhotoUrl,
                Story = entity.Story,
                Contest = contestDTO,
                User = userDTO,
            };
            return dto;
        }
        public override async Task<PhotoDTO> Update(PhotoDTO dto)
        {
            var photoDto = await GetById(dto.Id);
            var user = await _userservice.GetByUsername(photoDto.User.Username);
            var contest = await _contestservice.GetById(photoDto.Contest.Id);

            var userEntity = _mapper.Map<User>(user);
            var contestEntity = _mapper.Map<Contest>(contest);

            var photoEntity = new Photo()
            {
                Id = dto.Id,
                Title = dto.Title,
                PhotoFile = dto.PhotoFile,
                PhotoUrl = dto.PhotoUrl,
                ContestId = contestEntity.Id,
                UserId = userEntity.Id,
            };

            if (photoEntity == null)
            {
                throw new EntityNotFoundException();
            }

            _context.Set<Photo>().Update(photoEntity);
            await _context.SaveChangesAsync();

            return _mapper.Map<PhotoDTO>(photoEntity);
        }
        public void AddScore(int score, PhotoDTO photoDTO)
        {
            var entity =  _context.Set<Photo>().
                   AsNoTracking().FirstOrDefaultAsync(x => x.Id == photoDTO.Id);
            entity.Result.Scoring += (double)score;
            _context.Set<Photo>().Update(entity.Result);
            _context.SaveChangesAsync();
        }
    }
}
