﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using PhotoContest.Data.Entities;
using PhotoContest.Domain.Contracts.Repositories;
using PhotoContest.Domain.DataTransferObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhotoContest.Data.Repositories
{
    public class RatingRepository : BaseRepository<RatingDTO, Rating>, IRatingRepository
    {
        public RatingRepository(DataContext context, IMapper mapper) : base(context, mapper)
        {

        }

        public override async Task<RatingDTO> Create(RatingDTO dto)
        {
            var entity = _mapper.Map<Rating>(dto);

            await _context.Set<Rating>().AddAsync(entity);
            await _context.SaveChangesAsync();

            var result = _mapper.Map<RatingDTO>(entity);

            return result;
        }

        public override async Task<RatingDTO> GetById(int id)
        {
            var entity = await _context.Set<Rating>()
                .Include(r => r.Photo)
                .AsNoTracking().FirstOrDefaultAsync(x => x.Id == id);

            return _mapper.Map<RatingDTO>(entity);
        }
    }
}
