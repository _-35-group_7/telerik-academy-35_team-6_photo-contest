﻿using FluentValidation;
using PhotoContest.Domain.ViewModels.ContestViewModels;
using System;

namespace PhotoContest.Data.Validators
{
    public class ContestValidator : AbstractValidator<ContestPostViewModel>
    {
        public ContestValidator()
        {
            RuleFor(c => c.Title).NotEmpty().Length(6, 25);
            RuleFor(c => c.Category).NotEmpty().Length(6, 25);
            RuleFor(c => c.Phase).NotEmpty();
            RuleFor(c => c.Type).NotEmpty();
           //RuleFor(c => c.PhaseITerm).NotEmpty().Must(BeAValidDate).WithMessage("A valid deadline is required")
           //    .GreaterThanOrEqualTo(DateTime.Now.AddDays(1))
           //    .WithMessage("Please, create a contest with an expiration date longer or equal to 1 day.")
           //    .LessThanOrEqualTo(DateTime.Now.AddDays(30))
           //    .WithMessage("Please, create a contest with an expiration date less or equal to 30 days.");
           //RuleFor(c => c.PhaseIITerm).NotEmpty().Must(BeAValidDate).WithMessage("A valid deadline is required")
           //    .GreaterThanOrEqualTo(DateTime.Now.AddHours(1))
           //    .WithMessage("Please, insert a deadline longer than or equal to 1 hour.")
           //    .LessThanOrEqualTo(DateTime.Now.AddHours(24))
           //    .WithMessage("Please, insert a deadline less than or equal to 24 hours.");
        }

        private bool BeAValidDate(DateTime date)
        {
            return !date.Equals(default(DateTime));
        }

    }

    
}
