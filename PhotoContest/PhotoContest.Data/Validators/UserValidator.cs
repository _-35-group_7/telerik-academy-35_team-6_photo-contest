﻿using FluentValidation;
using PhotoContest.Domain.ViewModels;
using PhotoContest.Domain.ViewModels.UserViewModels;

namespace PhotoContest.Data.Validators
{
    public class UserValidator : AbstractValidator<UserPostViewModel>
    {
        public UserValidator()
        {
            RuleFor(u => u.FirstName).NotEmpty().Length(3,15);
            RuleFor(u => u.LastName).NotEmpty().Length(3, 15);
            RuleFor(u => u.Username).NotEmpty().Length(6, 20);

            RuleFor(p => p.Password).NotEmpty().WithMessage("Your password cannot be empty")
                    .MinimumLength(8).WithMessage("Your password length must be at least 8.")
                    .MaximumLength(25).WithMessage("Your password length must not exceed 16.")
                    .Matches(@"[A-Z]+").WithMessage("Your password must contain at least one uppercase letter.")
                    .Matches(@"[a-z]+").WithMessage("Your password must contain at least one lowercase letter.")
                    .Matches(@"[0-9]+").WithMessage("Your password must contain at least one number.")
                    .Matches(@"[\!\?\*\.]+").WithMessage("Your password must contain at least one (!? *.).");

            RuleFor(u => u.Email).NotEmpty().Length(3, 25).EmailAddress();
        } 

    }

    public class UserPutValidator : AbstractValidator<UserPutViewModel>
    {
        public UserPutValidator()
        {
            RuleFor(u => u.FirstName).NotEmpty().Length(3, 15);
            RuleFor(u => u.LastName).NotEmpty().Length(3, 15);

            RuleFor(p => p.Password).NotEmpty().WithMessage("Your password cannot be empty")
                    .MinimumLength(8).WithMessage("Your password length must be at least 8.")
                    .MaximumLength(25).WithMessage("Your password length must not exceed 16.")
                    .Matches(@"[A-Z]+").WithMessage("Your password must contain at least one uppercase letter.")
                    .Matches(@"[a-z]+").WithMessage("Your password must contain at least one lowercase letter.")
                    .Matches(@"[0-9]+").WithMessage("Your password must contain at least one number.")
                    .Matches(@"[\!\?\*\.]+").WithMessage("Your password must contain at least one (!? *.).");

            RuleFor(u => u.Email).NotEmpty().Length(3, 25).EmailAddress();
        }
    }

    public class UserRegisterValidator : AbstractValidator<UserRegPostModel>
    {
        public UserRegisterValidator()
        {
            RuleFor(u => u.FirstName).NotEmpty().Length(3, 15);
            RuleFor(u => u.LastName).NotEmpty().Length(3, 15);
            RuleFor(u => u.Username).NotEmpty().Length(6, 20);

            RuleFor(p => p.Password).NotEmpty().WithMessage("Your password cannot be empty")
                    .MinimumLength(8).WithMessage("Your password length must be at least 8.")
                    .MaximumLength(25).WithMessage("Your password length must not exceed 16.")
                    .Matches(@"[A-Z]+").WithMessage("Your password must contain at least one uppercase letter.")
                    .Matches(@"[a-z]+").WithMessage("Your password must contain at least one lowercase letter.")
                    .Matches(@"[0-9]+").WithMessage("Your password must contain at least one number.")
                    .Matches(@"[\!\?\*\.]+").WithMessage("Your password must contain at least one (!? *.).");
            RuleFor(p => p.Password).Matches(p => p.ConfirmPassword).WithMessage("Both passwords must match");

            RuleFor(u => u.Email).NotEmpty().Length(3, 25).EmailAddress();
        }
    }

    public class UserLoginValidator : AbstractValidator<UserLogPostModel>
    {
        public UserLoginValidator()
        {
            RuleFor(u => u.Username).NotEmpty().Length(6, 20);

            RuleFor(p => p.Password).NotEmpty().WithMessage("Your password cannot be empty")
                    .MinimumLength(8).WithMessage("Your password length must be at least 8.")
                    .MaximumLength(25).WithMessage("Your password length must not exceed 16.")
                    .Matches(@"[A-Z]+").WithMessage("Your password must contain at least one uppercase letter.")
                    .Matches(@"[a-z]+").WithMessage("Your password must contain at least one lowercase letter.")
                    .Matches(@"[0-9]+").WithMessage("Your password must contain at least one number.")
                    .Matches(@"[\!\?\*\.]+").WithMessage("Your password must contain at least one (!? *.).");

        }
    }
}
