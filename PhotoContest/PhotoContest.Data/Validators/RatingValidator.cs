﻿using FluentValidation;
using PhotoContest.Domain.ViewModels.RatingViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhotoContest.Data.Validators
{
    public class RatingValidator : AbstractValidator<RatingPostViewModel>
    {
        public RatingValidator()
        {
            RuleFor(r => r.Score).NotEmpty()
                .WithMessage("Please, insert a valid score! If a null score is given, insert '0'.");
            RuleFor(r => r.Comment).NotEmpty().WithMessage("Please, insert a comment.")
                .Length(10, int.MaxValue).WithMessage("The comment should be at least 10 symbols in legnth.");

        }
    }
}
