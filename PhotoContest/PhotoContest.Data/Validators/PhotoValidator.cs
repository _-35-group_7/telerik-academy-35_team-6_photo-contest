﻿using FluentValidation;
using PhotoContest.Domain.ViewModels.PhotoViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhotoContest.Data.Validators
{
    public class PhotoValidator : AbstractValidator<PhotoPostViewModel>
    {
        public PhotoValidator()
        {
            RuleFor(p => p.Title).NotEmpty().Length(5, 40)
                .WithMessage("Please, enter a title with at least 5 symbols length.");
            RuleFor(p => p.PhotoFile).NotEmpty();
            RuleFor(p => p.Story).NotEmpty().Length(10, int.MaxValue)
                .WithMessage("Please, enter a story with at least 10 symbols length.");
        }
    }
}
