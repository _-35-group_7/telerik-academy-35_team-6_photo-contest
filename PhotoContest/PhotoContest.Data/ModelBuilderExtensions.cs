﻿using Microsoft.EntityFrameworkCore;
using PhotoContest.Data.Entities;
using PhotoContest.Domain.Enums;
using System;
using System.Collections.Generic;
using System.Linq;

namespace PhotoContest.Data
{
    public static class ModelBuilderExtensions
    {
		public static void Seed(this ModelBuilder modelBuilder)
		{
			var photos = new List<Photo>();
			photos.Add(new Photo() { Title = "Cassy",
				PhotoFile = null,
				Story = "This is the first photo of my dog as a puppy.",
				Scoring = 0.0, Id = 1, ContestId = 1, UserId = 1 });
			photos.Add(new Photo() { Title = "Fidel",
				PhotoFile = null,
				Story = "I remember feeding this stray dog when I was a little child.",
				Scoring = 0.0,
				Id = 2, ContestId = 1, UserId = 2 });
			photos.Add(new Photo() { Title = "Mecho",
				PhotoFile = null,
				Story = "This is one of the first stray dogs that went to live near our villa, where my grandpa feeds it.",
				Scoring = 0.0,
				Id = 3, ContestId = 2, UserId = 1 });
			photos.Add(new Photo() { Title = "Liska",
				PhotoFile = null,
				Story = "This dog is one of the first mothers to have ever lived near my house.",
				Scoring = 0.0,
				Id = 4, ContestId = 2, UserId = 2 });

			modelBuilder.Entity<Photo>().HasData(photos);

			var contests = new List<Contest>();
			contests.Add(new Contest
			{
				Id = 1,
				Title = "First Contest",
				Category = "Dog pictures",
				TypeContest = TypeContest.Open,
				Phase =  Stage.PhaseII,
				PhaseITerm = DateTime.Now,
				PhaseIITerm = DateTime.Now.AddHours(12).AddMinutes(30)
			});
			contests.Add(new Contest
			{
				Id = 2,
				Title = "Second Contest",
				Category = "Cat pictures",
				TypeContest = TypeContest.Open,
				Phase = Stage.PhaseI,
				PhaseITerm = DateTime.Now.AddDays(12).AddHours(12),
				PhaseIITerm = DateTime.Now.AddDays(12).AddHours(18).AddMinutes(30)
			});
			
			modelBuilder.Entity<Contest>().HasData(contests);

			// Seed the database with users
			var users = new List<User>();
			users.Add(new User
			{
				Id = 1,
				FirstName = "Robert",
				LastName = "Rangel",
				Username = "r.rangel",
				Password = "R.rangel_96",
				Email = "r.rangel@abv.bg",
				Ranking = 0

			});
			users.Add(new User
			{
				Id = 2,
				FirstName = "Philip",
				LastName = "Fritz",
				Username = "p.fritz",
				Password = "P_fritz_96",
				Email = "p.fritz@abv.bg",
				Ranking = 0
			});

			modelBuilder.Entity<User>().HasData(users);

			var ratings = new List<Rating>();

			ratings.Add(new Rating
			{
				Id = 1,
				Score = 100,
				Comment = "This is a very cute picture!",
				IsReviewed = true,
				PhotoId = 1
			});

			modelBuilder.Entity<Rating>().HasData(ratings);
		}

	}
}
