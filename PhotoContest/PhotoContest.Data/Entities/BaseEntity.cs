﻿using FluentValidation;
using System;
using System.ComponentModel.DataAnnotations;

namespace PhotoContest.Data.Entities
{
    public abstract class BaseEntity
    {
        [Key]
        public int Id { get; set; }

        public DateTime CreatedOn { get; set; }
    }
}
