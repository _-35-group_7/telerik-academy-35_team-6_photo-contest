﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhotoContest.Data.Entities
{
    public class Rating : BaseEntity
    {
        public int Score { get; set; }
        public string Comment { get; set; }
        public bool IsReviewed { get; set; } = true;

        public int PhotoId { get; set; }
        public Photo Photo { get; set; }

    }
}
