﻿using PhotoContest.Domain.Enums;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace PhotoContest.Data.Entities
{
    public class User : BaseEntity
    {
        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Username { get; set; }

        public string Password { get; set; }

        public string Email { get; set; }

        public Role Role { get; set; } = Role.PhotoJunkie;

        public int Ranking { get; set; }

        public ICollection<Photo> Photos { get; set; } = new List<Photo>();

    }
}
