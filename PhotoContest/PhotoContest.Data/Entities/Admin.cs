﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhotoContest.Data.Entities
{
    public class Admin : BaseEntity
    {
        public string AdminName { get; set; }
        public string Password { get; set; }
        public byte[] PasswordSalt { get; set; }
    }
}
