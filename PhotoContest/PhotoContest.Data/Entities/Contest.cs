﻿using FluentValidation;
using PhotoContest.Domain.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace PhotoContest.Data.Entities
{
    public class Contest : BaseEntity
    {
        public string Title { get; set; }

        public string Category { get; set; }

        public TypeContest TypeContest { get; set; }

        public Stage Phase { get; set; }

        public DateTime PhaseITerm { get; set; }

        public DateTime PhaseIITerm { get; set; }

        public ICollection<Photo> Photos { get; set; } = new List<Photo>();

        public ICollection<User> Partakers { get; set; } = new List<User>();

    }

}
