﻿using Microsoft.AspNetCore.Http;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PhotoContest.Data.Entities
{
    public class Photo : BaseEntity
    {
        public string Title { get; set; }

        [Display(Name = "Choose your photo for upload")]
        [NotMapped]
        public IFormFile PhotoFile { get; set; }

        public string PhotoUrl { get; set; }

        public string Story { get; set; }

        public double Scoring { get; set; }

        public int UserId { get; set; }
        public User User { get; set; }

        public int ContestId { get; set; }
        public Contest Contest { get; set; }

        public ICollection<Rating> Ratings { get; set; }  = new List<Rating>();
    }
}
