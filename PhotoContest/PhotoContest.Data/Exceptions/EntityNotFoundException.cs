﻿using System;

namespace PhotoContest.Data.Exceptions
{
	public class EntityNotFoundException : ApplicationException
	{
		public EntityNotFoundException(string message)
			: base(message)
		{

		}

		public EntityNotFoundException()
        {

        }
	}
}
