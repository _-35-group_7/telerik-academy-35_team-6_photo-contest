﻿using System;


namespace PhotoContest.Data.Exceptions
{
    public class InvalidEntryException : ApplicationException
    {
        public InvalidEntryException(string message)
            : base(message)
        {

        }
    }
}
