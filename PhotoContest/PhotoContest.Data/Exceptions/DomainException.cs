﻿using System;


namespace PhotoContest.Data.Exceptions
{
    public class DomainException : Exception
    {
        public DomainException(string message)
            : base(message)
        {

        }
    }
}
