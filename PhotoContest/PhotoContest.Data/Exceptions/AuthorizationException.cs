﻿using System;

namespace PhotoContest.Data.Exceptions
{
	public class AuthorizationException : ApplicationException
	{
		public AuthorizationException(string message)
			: base(message)
		{
		}
	}
}
