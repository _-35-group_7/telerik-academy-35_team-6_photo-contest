﻿using System;

namespace PhotoContest.Data.Exceptions
{
    public class AuthenticationException : ApplicationException
    {
        public AuthenticationException(string message)
            : base(message)
        {
        }
    }
}
