﻿using System;

namespace PhotoContest.Data.Exceptions
{
	public class UnauthorizedOperationException : ApplicationException
	{
		public UnauthorizedOperationException(string message)
			: base(message)
		{
		}
	}
}
