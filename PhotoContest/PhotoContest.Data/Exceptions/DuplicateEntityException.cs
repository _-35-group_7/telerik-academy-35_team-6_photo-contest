﻿using System;

namespace PhotoContest.Data.Exceptions
{
	public class DuplicateEntityException : ApplicationException
	{
		public DuplicateEntityException(string message)
			: base(message)
        {

        }

		public DuplicateEntityException()
        {

        }
	}
}
