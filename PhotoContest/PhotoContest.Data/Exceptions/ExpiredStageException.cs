﻿using System;


namespace PhotoContest.Data.Exceptions
{
    public class ExpiredStageException : ApplicationException
    {
        public ExpiredStageException(string message)
            : base(message)
        {

        }
    }
}
