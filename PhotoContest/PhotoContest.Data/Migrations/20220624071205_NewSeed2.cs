﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace PhotoContest.Data.Migrations
{
    public partial class NewSeed2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "Contests",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "PhaseIITerm", "PhaseITerm" },
                values: new object[] { new DateTime(2022, 6, 24, 22, 42, 4, 250, DateTimeKind.Local).AddTicks(3087), new DateTime(2022, 6, 24, 10, 12, 4, 248, DateTimeKind.Local).AddTicks(89) });

            migrationBuilder.UpdateData(
                table: "Contests",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "PhaseIITerm", "PhaseITerm" },
                values: new object[] { new DateTime(2022, 7, 7, 4, 42, 4, 250, DateTimeKind.Local).AddTicks(3563), new DateTime(2022, 7, 6, 22, 12, 4, 250, DateTimeKind.Local).AddTicks(3543) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                column: "Role",
                value: 2);

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 2,
                column: "Role",
                value: 2);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "Contests",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "PhaseIITerm", "PhaseITerm" },
                values: new object[] { new DateTime(2022, 6, 24, 22, 37, 54, 270, DateTimeKind.Local).AddTicks(1906), new DateTime(2022, 6, 24, 10, 7, 54, 266, DateTimeKind.Local).AddTicks(3459) });

            migrationBuilder.UpdateData(
                table: "Contests",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "PhaseIITerm", "PhaseITerm" },
                values: new object[] { new DateTime(2022, 7, 7, 4, 37, 54, 270, DateTimeKind.Local).AddTicks(2384), new DateTime(2022, 7, 6, 22, 7, 54, 270, DateTimeKind.Local).AddTicks(2364) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                column: "Role",
                value: 1);

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 2,
                column: "Role",
                value: 1);
        }
    }
}
