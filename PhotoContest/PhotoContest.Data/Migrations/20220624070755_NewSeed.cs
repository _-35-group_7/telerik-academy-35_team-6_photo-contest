﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace PhotoContest.Data.Migrations
{
    public partial class NewSeed : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "Contests",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "PhaseIITerm", "PhaseITerm" },
                values: new object[] { new DateTime(2022, 6, 24, 22, 37, 54, 270, DateTimeKind.Local).AddTicks(1906), new DateTime(2022, 6, 24, 10, 7, 54, 266, DateTimeKind.Local).AddTicks(3459) });

            migrationBuilder.UpdateData(
                table: "Contests",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "PhaseIITerm", "PhaseITerm" },
                values: new object[] { new DateTime(2022, 7, 7, 4, 37, 54, 270, DateTimeKind.Local).AddTicks(2384), new DateTime(2022, 7, 6, 22, 7, 54, 270, DateTimeKind.Local).AddTicks(2364) });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "Contests",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "PhaseIITerm", "PhaseITerm" },
                values: new object[] { new DateTime(2022, 6, 24, 22, 22, 35, 981, DateTimeKind.Local).AddTicks(4494), new DateTime(2022, 6, 24, 9, 52, 35, 978, DateTimeKind.Local).AddTicks(8403) });

            migrationBuilder.UpdateData(
                table: "Contests",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "PhaseIITerm", "PhaseITerm" },
                values: new object[] { new DateTime(2022, 7, 7, 4, 22, 35, 981, DateTimeKind.Local).AddTicks(4817), new DateTime(2022, 7, 6, 21, 52, 35, 981, DateTimeKind.Local).AddTicks(4803) });
        }
    }
}
